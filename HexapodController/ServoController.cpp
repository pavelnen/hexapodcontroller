#include <iomanip>
#include "ServoController.h"
#include "PortController.h"
using namespace Hexapod;

#ifdef HEXAPOD_USE_REAL_SERVOS
PhysicalServoController::PhysicalServoController(const PhysicalServoDescriptor *desc)
	:_servoDesc(*desc), _isFirstMove(true), _targetAngle(0.0) {
	*_commandBuffer = '\0';
}

void PhysicalServoController::setAngle(double angle) {
	_targetAngle = angle;
	_isFirstMove = false;
	std::sprintf(_commandBuffer, "#%d P%d\r", _servoDesc.ssc32channelIndex, _impulsWidthForAngle(angle));
	_flushCommand();
}

void PhysicalServoController::setAngleWithDuration(double angle, double duration) {
	if (_isFirstMove) {
		if (kVerbosityLevel >= kVerbosityMedium) {
			std::cout << "Ignoring first movement duration with destination angle: "
				<< std::setiosflags(std::ios::fixed) << std::setprecision(4) << angle 
				<< "at channel: " << _servoDesc.ssc32channelIndex << "\n";
		}
		setAngle(angle);
	}
	else {
		_targetAngle = angle;
		int milliseconds = (int)(duration * 1000);
		assert(milliseconds >= 0);
		std::sprintf(_commandBuffer, "#%d P%d T%d", _servoDesc.ssc32channelIndex, _impulsWidthForAngle(angle), milliseconds);
		_flushCommand();
	}
}
void PhysicalServoController::setAngleWithSpeed(double angle, double speed) {
	if (_isFirstMove) {
		if (kVerbosityLevel >= kVerbosityMedium) {
			std::cout << "Ignoring first movement speed with destination angle: " 
				<< std::setiosflags(std::ios::fixed) << std::setprecision(4) << angle 
				<< " at channel: " << _servoDesc.ssc32channelIndex << "\n";
		}
		setAngle(angle);
	}
	else {
		_targetAngle = angle;
		int speed = (int)(speed/ a180 * kImpulsRange);
		assert(speed >= 0);
		std::sprintf(_commandBuffer, "#%d P%d S%d\r", _servoDesc.ssc32channelIndex, _impulsWidthForAngle(angle), speed);
		_flushCommand();
	}
}

double PhysicalServoController::getMinAngle() const {
	// TODO invertion
	return ((double)(_servoDesc.impulsMin - kImpulsStart - _servoDesc.impulsCorrection)) / kImpulsRange * a180 - a90;
}

double PhysicalServoController::getMaxAngle() const {
	// TODO invertion
	return ((double)(_servoDesc.impulsMax - kImpulsStart - _servoDesc.impulsCorrection)) / kImpulsRange * a180 - a90;
}

int PhysicalServoController::_impulsWidthForAngle(double angle) const {
	if (_servoDesc.inverted) {
		angle = -angle;
	}
	angle = (angle + a90) / a180; // convert (-a90, a90) interval to (0.0, 1.0)
	int impuls = kImpulsStart + (int)(kImpulsRange * angle);
	int correctedImpuls = Constrain(impuls + _servoDesc.impulsCorrection, _servoDesc.impulsMin, _servoDesc.impulsMax);
	return correctedImpuls;
}

void PhysicalServoController::_flushCommand() {
	PortController::GetControllerForPortHandle(_servoDesc.hPort)->appendMessage(_commandBuffer);
	_commandBuffer[0] = '\0';
}

#endif

#ifdef HEXAPOD_USE_WEBOTS_SERVOS
const double WebotsServoController::kMaxVelocity = 5.24; // radians per second

WebotsServoController::WebotsServoController(const WebotsServoDescriptor *desc)
	:_servoDesc(*desc), _targetAngle(0.0), _speed(kMaxVelocity) {
		_servo = ((webots::Robot*)(_servoDesc.robot))->getServo(_servoDesc.name);
}

void WebotsServoController::setAngle(double angle) {
	_targetAngle = Constrain(angle, _servoDesc.angleMin, _servoDesc.angleMax);
	//_servo->setVelocity(kMaxVelocity);
	_servo->setPosition(_targetAngle);
}

void WebotsServoController::setAngleWithDuration(double angle, double duration) {
	_targetAngle = Constrain(angle, _servoDesc.angleMin, _servoDesc.angleMax);
	double angleDelta = _targetAngle - _servo->getPosition();
	double velocity = Constrain(std::abs(angleDelta / duration), 0.0, kMaxVelocity);
	//_servo->setVelocity(velocity);
	_servo->setPosition(_targetAngle);
}

void WebotsServoController::setAngleWithSpeed(double angle, double speed) {
	_targetAngle = Constrain(angle, _servoDesc.angleMin, _servoDesc.angleMax);
	double velocity = Constrain(speed, 0.0, kMaxVelocity);
	//_servo->setVelocity(velocity);
	_servo->setPosition(_targetAngle);
}
	
#endif

ServoController::ServoController() {
	_physicalServo = NULL;
	_webotsServo = NULL;
}

ServoController::~ServoController() {
	SAFE_DELETE(_physicalServo);
	SAFE_DELETE(_webotsServo);
}

void ServoController::initPhysicalServo(const PhysicalServoDescriptor *desc) {
#ifdef HEXAPOD_USE_REAL_SERVOS
	assert(_physicalServo == NULL);
	_physicalServo = new PhysicalServoController(desc);
#endif
}

void ServoController::initWebotsServo(const WebotsServoDescriptor *desc) {
#ifdef HEXAPOD_USE_WEBOTS_SERVOS
	assert(_webotsServo == NULL);
	_webotsServo = new WebotsServoController(desc);
#endif
}

void ServoController::_initializationCheck() const {
#ifdef HEXAPOD_USE_REAL_SERVOS
	assert(_physicalServo != NULL);
#endif
#ifdef HEXAPOD_USE_WEBOTS_SERVOS
	assert(_webotsServo != NULL);
#endif
}

double ServoController::getTargetAngle() const {
	_initializationCheck();
#ifdef HEXAPOD_USE_REAL_SERVOS
	return _physicalServo->getTargetAngle();
#else
  #ifdef HEXAPOD_USE_WEBOTS_SERVOS
	return _webotsServo->getTargetAngle();
  #elif
	return 0.0;
  #endif
#endif
}

double ServoController::getMinAngle() const {
	_initializationCheck();
#ifdef HEXAPOD_USE_REAL_SERVOS
	return _physicalServo->getMinAngle();
#else
  #ifdef HEXAPOD_USE_WEBOTS_SERVOS
	return _webotsServo->getMinAngle();
  #elif
	return 0.0;
  #endif
#endif
}

double ServoController::getMaxAngle() const {
	_initializationCheck();
#ifdef HEXAPOD_USE_REAL_SERVOS
	return _physicalServo->getMaxAngle();
#else
  #ifdef HEXAPOD_USE_WEBOTS_SERVOS
	return _webotsServo->getMaxAngle();
  #elif
	return 0.0;
  #endif
#endif
}

void ServoController::setAngle(double angle) {
	_initializationCheck();
#ifdef HEXAPOD_USE_REAL_SERVOS
	_physicalServo->setAngle(angle);
#endif
#ifdef  HEXAPOD_USE_WEBOTS_SERVOS
	 _webotsServo->setAngle(angle);
#endif
}

void ServoController::setAngleWithDuration(double angle, double duration) {
	_initializationCheck();
#ifdef HEXAPOD_USE_REAL_SERVOS
	_physicalServo->setAngleWithDuration(angle, duration);
#endif
#ifdef  HEXAPOD_USE_WEBOTS_SERVOS
	_webotsServo->setAngleWithDuration(angle, duration);
#endif
}

void ServoController::setAngleWithSpeed(double angle, double speed) {
	_initializationCheck();
#ifdef HEXAPOD_USE_REAL_SERVOS
	_physicalServo->setAngleWithSpeed(angle, speed);
#endif
#ifdef  HEXAPOD_USE_WEBOTS_SERVOS
	_webotsServo->setAngleWithSpeed(angle, speed);
#endif
}