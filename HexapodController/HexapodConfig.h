#ifndef __HEXAPOD_HEXAPOD_CONFIG_H__
#define __HEXAPOD_HEXAPOD_CONFIG_H__

#define HEXAPOD_USE_WEBOTS_SERVOS
#define HEXAPOD_USE_WEBOTS_SENSORS
//#define HEXAPOD_USE_REAL_SERVOS
//#define HEXAPOD_USE_REAL_SENSORS
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
  #undef HEXAPOD_USE_REAL_SENSORS
  #define HEXAPOD_USE_WEBOTS_SENSORS
#endif

#include "GeometricPrimitives.h"

namespace Hexapod {
	extern const int kTimeStep; // ms

	typedef int LegIndex;
	extern const LegIndex kFirstLeg;
	extern const LegIndex FL;
	extern const LegIndex ML;
	extern const LegIndex RL;
	extern const LegIndex RR;
	extern const LegIndex MR;
	extern const LegIndex FR;
	extern const LegIndex kLastLeg;
	const int kLegsCount = 6;
	extern const double kLegsDirections [6];

	typedef int LegServoIndex;
	extern const LegServoIndex SH;
	extern const LegServoIndex SV;
	extern const LegServoIndex KN;
	const int kLegServosCount = 3;

	// Lengths in meters
	extern const double kShoulderLength;
	extern const double kShoulderHeight;
	extern const double kFemurLength;
	extern const double kTibiaLength;
	extern const double kBodyRadius;
	extern const double kBodyHeight;
	extern const double kTipRadialOffset;
	extern const double kTipRadialDistance;
	extern const double kBodyCenterToShoulderVerticalOffset;

	extern const char *kServoNames [6][3];
	extern const char *kTouchesNames [6];
}

#endif