#ifndef __HEXAPOD_GENERIC_H__
#define __HEXAPOD_GENERIC_H__

#include <iostream>
#include "assert.h"
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include "windows.h"
//#include "HexapodConfig.h"

namespace Hexapod {
	using std::cos; 
	using std::acos;
	using std::sin;
	using std::asin;
	extern const double kDistanceEpsilon;
	extern const double kTimeEpsilon;

	// 1 for errors and issues (~0-10 per process life)
	// 2 for important states changes (1 time per ~1-10 secs)
	// 3 for every move  ~1-10 times per time step
	extern const int kVerbosityOff;
	extern const int kVerbosityLow;
	extern const int kVerbosityMedium;
	extern const int kVerbosityHigh;
	extern const int kVerbosityLevel;

	extern const double a15, a30, a45, a60, a90, a120, a150, a180, a360;
	double NormalizedAngle(double angle);

#define SAFE_DELETE(_X) if ((_X) != NULL) { delete (_X); (_X) = NULL;}

	void Print(const char *msg, int verbosityLevel = 1);
	void Warn(const char *msg);
	void Error(const char *msg);

	template <class T>
	inline T Constrain(T value, T minValue, T maxValue) {
		return std::max(std::min(value, maxValue), minValue);
	}

	template <class T>
	inline T Sqr(T x) {
		return x*x;
	}
}

#endif