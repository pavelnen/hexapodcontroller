#include "Generic.h"
using namespace Hexapod;

const double Hexapod::kDistanceEpsilon = 0.001; // 1 mm
const double Hexapod::kTimeEpsilon = 0.001; // 1ms

const int Hexapod::kVerbosityOff = 0;
const int Hexapod::kVerbosityLow = 1;
const int Hexapod::kVerbosityMedium = 2;
const int Hexapod::kVerbosityHigh = 3;
const int Hexapod::kVerbosityLevel = kVerbosityHigh;

const double Hexapod::a360 = 3.1415926*2;
const double Hexapod::a180 = 3.1415926;
const double Hexapod::a150 = 5*a180/6;
const double Hexapod::a120 = 4*a180/6;
const double Hexapod::a90 = a180/2;
const double Hexapod::a60 = a180/3;
const double Hexapod::a45 = a180/4;
const double Hexapod::a30 = a180/6;
const double Hexapod::a15 = a180/12;

double Hexapod::NormalizedAngle(double angle) { // Returns angle within -a180 to a180 interval
	while (angle > a180) angle -= a360;
	while (angle < -a180) angle += a360;
	return angle;
}

void Hexapod::Print(const char *msg, int verbosityLevel) {
	if (kVerbosityLevel >= kVerbosityLow) {
		std::cout << "HP: !!! WARNING: " << msg << "\n";
	}
}

void Hexapod::Warn(const char *msg) {
	if (kVerbosityLevel >= kVerbosityLow) {
		std::cout << "HP: !!! WARNING: " << msg << "\n";
	}
}

void Hexapod::Error(const char *msg) {
	std::cout << "HP: !!! ERROR: " << msg << "\n";
	assert(false);
}