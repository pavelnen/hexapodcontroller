#include "HexapodController.h"

#if defined(HEXAPOD_USE_WEBOTS_SERVOS) || defined(HEXAPOD_USE_WEBOTS_SENSORS)
  #include <webots/Robot.hpp>
  #include <webots/Servo.hpp>
#endif

using namespace Hexapod;

HexapodController::HexapodController(HANDLE hPort
				, PhysicalServoDescriptor ppsd[][3], WebotsServoDescriptor pwsd[][3]
				, PhysicalSensorDescriptor *pptd, WebotsSensorDescriptor *pwtd
				, PhysicalSensorDescriptor *ppad, WebotsSensorDescriptor *pwad) 
{
		_hPort = hPort;	

		CoordinateSystem bodyCS(Vector(0.0), &kGlobalCS);
		_body = new BodyController(bodyCS);

		for (LegIndex leg = kFirstLeg; leg <= kLastLeg; ++leg) {
			Vector tipOffset( kTipRadialDistance * std::cos(kLegsDirections[leg]), kTipRadialDistance* std::sin(kLegsDirections[leg]), -kBodyHeight);
			CoordinateSystem tipCS(tipOffset, &kGlobalCS);
			_legs[leg] = new LegController(_body->shoulderCS(leg), tipCS);
			_legs[leg]->initWebotsServos(pwsd[leg]);
			_legs[leg]->initWebotsSensors(&(pwtd[leg]), NULL);
			_legs[leg]->setStepHeight(0.12);
			_legs[leg]->setStepMode(kLegStepModeTouchAware);
		}
		_body->setLegControllers(_legs);
		_accelerometer.initWebotsAccelerometer(pwad);

		_duopodInd = 0;
}

Vector HexapodController::_rotationVectorForLegsPositions(const Vector *legsPositions, const bool *activeLegs, const Vector &rotationCenter) {
	int activeLegsCount = 0;
	Vector rotationVectorSum(0.0);
	double rotationWeightSum = 0.001;
	for (LegIndex leg = 0; leg < kLegsCount; ++leg) {
		if (activeLegs[leg]) {
			activeLegsCount += 1;
			Vector delta = legsPositions[leg] - rotationCenter;
			Vector defaultLegPosition(kTipRadialDistance * std::cos(kLegsDirections[leg]), kTipRadialDistance * std::sin(kLegsDirections[leg]), -kBodyHeight);
			Vector defaultDelta = defaultLegPosition - rotationCenter;
			Vector rotationVector = defaultDelta.cross(delta) / (delta.length() + 0.001) / (defaultDelta.length() + 0.001); // Add 1mm to avoid division by zero
			double rotationWeight = delta.length();
			rotationWeightSum += delta.length();
			rotationVectorSum += rotationVector * rotationWeight;
		}
	}
	if (activeLegsCount > 0) {
		return rotationVectorSum / rotationWeightSum;
	}
	else {
		return Vector(0.0, 0.0, 0.0);
	}
}

Vector HexapodController::_legsCenterForLegsPositions(const Vector *legsPositions, const bool *activeLegs) {
	int activeLegsCount = 0;
	Vector center(0.0);
	for (LegIndex leg = kFirstLeg; leg <= kLastLeg; ++leg) {
		if (activeLegs[leg]) {
			activeLegsCount += 1;
			center += legsPositions[leg];
		}
	}
	if (activeLegsCount > 0) {
		center /= (double)activeLegsCount;
	}
	return center;
};

void HexapodController::_recoveryDeltasRelativeToTip(const bool *activeLegs, Vector *deltasOut) {
	Vector legsPositions[kLegsCount];
	for (LegIndex leg = kFirstLeg; leg <= kLastLeg; ++leg) {
		if (activeLegs[leg]) {
			legsPositions[leg] = kGlobalCS.convertVectorFromCS(_legs[leg]->tipCS(), _legs[leg]->tipPosition());
		}
	}
	Vector legsCenter = _legsCenterForLegsPositions(legsPositions, activeLegs);
	Vector legsDelta = Vector(0.0, 0.0, -kBodyHeight) - legsCenter;
	Vector rotationVector = -_rotationVectorForLegsPositions(legsPositions, activeLegs, legsCenter);
	CoordinateSystem rotationCS = CoordinateSystem::rotationCS(rotationVector);
	for (LegIndex leg = kFirstLeg; leg <= kLastLeg; ++leg) {
		Vector legToCenterOffset = legsPositions[leg] - legsCenter;
		Vector recoveredLegToCenterOffset = rotationCS * legToCenterOffset + legsDelta;
		Vector legToCenterOffsetRelativeToTip = _legs[leg]->tipCS().convertVectorFromCS(kGlobalCS, legToCenterOffset);
		Vector legToCenterOffsetRecoveredRelativeToTip = _legs[leg]->tipCS().convertVectorFromCS(kGlobalCS, recoveredLegToCenterOffset);
		Vector recoveryDelta = legToCenterOffsetRecoveredRelativeToTip - legToCenterOffsetRelativeToTip;
		deltasOut[leg] = recoveryDelta;
	}
}

void HexapodController::setLegPositionWithDuration(LegIndex leg, const Vector &position, double duration) {
	_legs[leg]->setTipPosition(position, duration);
}

void HexapodController::stopManualControlAll() {
	for (LegIndex leg = kFirstLeg; leg != kLastLeg; ++leg) {
		_legs[leg]->stopManualControl();
	}
}

void HexapodController::update(double deltaTime) {
	//_accelerometer.value().affineVector().print("acc");
	//Vector gn = kGlobalCS.convertVectorFromCS(_body->cs(), _accelerometer.value()).normalized();
	Vector gn = _accelerometer.value().normalized();
	Vector newTipZAxis(gn);
	Vector newTipXAxis = (Vector(1.0, 0.0, 0.0) - gn * gn.dot(Vector(1.0, 0.0, 0.0))).normalized();
	Vector newTipYAxis = newTipZAxis.cross(newTipXAxis);
	for (LegIndex leg = kFirstLeg; leg <= kLastLeg; ++leg) {
		Vector offset = _legs[leg]->tipCS().offset();
		CoordinateSystem newTipCS(newTipXAxis, newTipYAxis, newTipZAxis, offset, _legs[leg]->tipCS().parentCS());
		if (leg == FL) {
			static int c = 0; if ((++c)%50 == 0) newTipCS.affineMatrix().print("FL");
		}
		_legs[leg]->setTipCS(newTipCS);
	}

	_accelerometer.update(kTimeStep / 1000.0);
	double xScale = 0.02f;
	//_body->setPosition(Vector(gn.x * 0.06, gn.y * 0.06, xScale *_rstick.y));
	bool activeLegs[] = {true, true, true, true, true, true};
	Vector legsPositions[kLegsCount];
	for (LegIndex leg = kFirstLeg; leg <= kLastLeg; ++leg) {
		if (activeLegs[leg]) {
			legsPositions[leg] = kGlobalCS.convertVectorFromCS(_legs[leg]->tipCS(), _legs[leg]->tipPosition());
		}
	}
	Vector legsCenter = _legsCenterForLegsPositions(legsPositions, activeLegs);
	Vector rotationVector = _rotationVectorForLegsPositions(legsPositions, activeLegs, legsCenter);
	_body->alignBy(rotationVector, legsCenter);

	for (LegIndex leg = kFirstLeg; leg <= kLastLeg; ++leg) {
		_legs[leg]->update(kTimeStep / 1000.0);
	}
	/* if ( ! PortController::GetControllerForPortHandle(_hPort)->isEmpty() ) {
	PortController::GetControllerForPortHandle(_hPort)->appendMessage("\r");
	PortController::GetControllerForPortHandle(_hPort)->flush();
	} */

	bool allSteady = true;
	for (LegIndex leg = kFirstLeg; leg <= kLastLeg; ++leg) {
		if ( ! _legs[leg]->isSteady()) {
			allSteady = false;
			break;
		}
	}
	if (allSteady) {
		LegIndex duopods[3][2] = { {FR, RL}, {ML, MR}, {RR, FL} };
		for (int i = 0; i < 2; ++i) {
			//_legs[duopods[_duopodInd][i]]->makeStep(Vector(0.04 * gn.z * gn.z, 0.0, 0.0), 1.5);
			_legs[duopods[_duopodInd][i]]->makeStep(_movementSpeed, 1.5);
		}
		bool steadyLegs[] = {_duopodInd != 2, _duopodInd != 1, _duopodInd != 0, _duopodInd != 2, _duopodInd != 1, _duopodInd != 0};
		Vector recoveryDeltas[kLegsCount];
		_recoveryDeltasRelativeToTip(steadyLegs, recoveryDeltas);
		Vector legsCenter = _body->averageLegsPositionRelativeToBody(steadyLegs);
		int nextDuopodInd = (_duopodInd + 1) % 3;
		int prevDuopodInd = (_duopodInd + 2) % 3;
		for (int i = 0; i < 2; ++i) {
			LegIndex nextLeg = duopods[nextDuopodInd][i];
			//Vector movementDelta1 = Vector(-0.015 * gn.z * gn.z, 0.0, 0.0) + recoveryDeltas[nextLeg] + _legs[nextLeg]->tipCS().offset();
			Vector movementDelta1 = -_movementSpeed/2 + recoveryDeltas[nextLeg] + _legs[nextLeg]->tipCS().offset();
			movementDelta1 = _legs[nextLeg]->tipCS().convertVectorFromCS(kGlobalCS, movementDelta1);
			_legs[nextLeg]->moveBy(movementDelta1, 1.0);
			LegIndex prevLeg = duopods[prevDuopodInd][i];
			Vector movementDelta2 = -_movementSpeed/2 + recoveryDeltas[prevLeg] + _legs[prevLeg]->tipCS().offset();
			movementDelta2 = _legs[prevLeg]->tipCS().convertVectorFromCS(kGlobalCS, movementDelta2);
			_legs[prevLeg]->moveBy(movementDelta2, 1.0);
		}
		_duopodInd = (_duopodInd + 1) % 3;
	}
}