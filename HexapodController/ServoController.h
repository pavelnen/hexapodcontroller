#ifndef __HEXAPOD_SERVO_CONTROLLER_H__
#define __HEXAPOD_SERVO_CONTROLLER_H__

#include "Generic.h"
#include "HexapodConfig.h"
#ifdef HEXAPOD_USE_WEBOTS_SERVOS
  #include <webots/Robot.hpp>
  #include <webots/Servo.hpp>
#endif

namespace Hexapod {
	class ServoControllerProtocol {
		virtual double getTargetAngle() const = 0;
		virtual double getMinAngle() const = 0;
		virtual double getMaxAngle() const = 0;
		virtual void setAngle(double angle) = 0;
		virtual void setAngleWithDuration(double angle, double duration) = 0;
		virtual void setAngleWithSpeed(double angle, double speed) = 0;
	};

	struct PhysicalServoDescriptor {
		int ssc32channelIndex;
		int impulsMin;
		int impulsMax;
		int impulsCorrection;
		bool inverted;
		HANDLE hPort;
	};

#ifdef HEXAPOD_USE_REAL_SERVOS
	class PhysicalServoController: public ServoControllerProtocol {
	public:
		PhysicalServoController(const PhysicalServoDescriptor *desc);
		virtual double getTargetAngle() const {return _targetAngle;}
		virtual double getMinAngle() const;
		virtual double getMaxAngle() const;
		virtual void setAngle(double angle);
		virtual void setAngleWithDuration(double angle, double duration);
		virtual void setAngleWithSpeed(double angle, double speed);
	private:
		int _impulsWidthForAngle(double angle) const;
		void _flushCommand();
	private:
		static const int kImpulsAbsoluteMin = 500;
		static const int kImpulsRange = 1800; // 180 degrees range
		static const int kImpulsStart = 600; // -90 degrees according to HS-645MG specs (www.servocity.com)
		static const int kImpulsAbsoluteMax = 2500;
		char _commandBuffer[128];
		bool _isFirstMove;
		PhysicalServoDescriptor _servoDesc;
		double _targetAngle;
	};
#else
	class PhysicalServoController { };
#endif

	struct WebotsServoDescriptor {
		const char *name;
		void *robot;
		double angleMin;
		double angleMax;
	};

#ifdef HEXAPOD_USE_WEBOTS_SERVOS
	class WebotsServoController: public ServoControllerProtocol {
	public:
		WebotsServoController(const WebotsServoDescriptor *desc);
		virtual double getTargetAngle() const {return _targetAngle;}
		virtual double getMinAngle() const {return _servoDesc.angleMin;}
		virtual double getMaxAngle() const {return _servoDesc.angleMax;}
		virtual void setAngle(double angle);
		virtual void setAngleWithDuration(double angle, double duration);
		virtual void setAngleWithSpeed(double angle, double speed);
	private:
		static const double kMaxVelocity;
		webots::Servo *_servo;
		WebotsServoDescriptor _servoDesc;
		double _targetAngle;
		double _speed;
	};
#else
	class WebotsServoController { };
#endif

	class ServoController: public ServoControllerProtocol {
	public:
		ServoController();
		~ServoController();
		void initPhysicalServo(const PhysicalServoDescriptor *desc);
		PhysicalServoController *getPhysicalServo() {return _physicalServo;}
		void initWebotsServo(const WebotsServoDescriptor *desc);
		WebotsServoController *getWebotsServo() {return _webotsServo;}
		virtual double getTargetAngle() const;
		virtual double getMinAngle() const;
		virtual double getMaxAngle() const;
		virtual void setAngle(double angle);
		virtual void setAngleWithDuration(double angle, double duration);
		virtual void setAngleWithSpeed(double angle, double speed);
	private:
		void _initializationCheck() const;
	private:
		PhysicalServoController *_physicalServo;
		WebotsServoController *_webotsServo;
	};
}

#endif