#include "LegPositioner.h"
#include "HexapodConfig.h"
#include "Movement.h"
using namespace Hexapod;

LegPositioner::LegPositioner(const CoordinateSystem *pShoulderCS, const CoordinateSystem &tipCS)
	:_pShoulderCS(pShoulderCS), _tipCS(tipCS) {
		_pMovement = NULL;
		_positioningDurationLeft = -1.0f;
}
void LegPositioner::initPhysicalServos(const PhysicalServoDescriptor *descs) {
	for (int i = 0; i < kLegServosCount; ++i) {
		_servoControllers[i].initPhysicalServo(&(descs[i]));
	}
}
void LegPositioner::initWebotsServos(const WebotsServoDescriptor *descs) {
	for (int i = 0; i < kLegServosCount; ++i) {
		_servoControllers[i].initWebotsServo(&(descs[i]));
	}
	_calculateTipArea();
}
void LegPositioner::_calculateTipArea() {
	static bool showOnce = true;
	_area.shAngleMin = _servoControllers[0].getMinAngle();
	_area.shAngleMax = _servoControllers[0].getMaxAngle();
	_area.svAngleMin = _servoControllers[1].getMinAngle();
	_area.svAngleMax = _servoControllers[1].getMaxAngle();
	_area.knAngleMin = _servoControllers[2].getMinAngle() - a90;
	_area.knAngleMax = _servoControllers[2].getMaxAngle() - a90;
	_area.bendedLegLength = (Vector(kFemurLength) + Vector(kTibiaLength, _area.knAngleMin).fromPolar()).length();
	_area.bendedLegAngleOffset = -acos( (kFemurLength + cos(_area.knAngleMin)*kTibiaLength) / _area.bendedLegLength );
	_area.unbendedLegLength = (Vector(kFemurLength) + Vector(kTibiaLength, _area.knAngleMax).fromPolar()).length();
	_area.unbendedLegAngleOffset = -acos( (kFemurLength + cos(_area.knAngleMax)*kTibiaLength) / _area.unbendedLegLength );
	_area.angle1 = _area.svAngleMax + _area.unbendedLegAngleOffset;
	_area.angle2 = _area.svAngleMax + _area.bendedLegAngleOffset;
	_area.angle3 = _area.svAngleMin + _area.unbendedLegAngleOffset;
	_area.angle4 = _area.svAngleMin + _area.bendedLegAngleOffset;
	_area.point1 = Vector(_area.unbendedLegLength, _area.angle1).fromPolar();
	_area.point2 = Vector(_area.bendedLegLength, _area.angle2).fromPolar();
	_area.point3 = Vector(_area.unbendedLegLength, _area.angle3).fromPolar();
	_area.point4 = Vector(_area.bendedLegLength, _area.angle4).fromPolar();
	_area.point5 = Vector(kFemurLength, _area.svAngleMin).fromPolar();
	_area.point6 = Vector(kFemurLength, _area.svAngleMax).fromPolar();
	if (showOnce) {
		std::cout << "AreaDesc\nBEGIN\n" << _area.shAngleMin << "  " << _area.shAngleMax << "\n"
			<< _area.svAngleMin << "  " << _area.svAngleMax << "\n"
			<< _area.knAngleMin << "  " << _area.knAngleMax << "\nb_u:\n"
			<< _area.bendedLegLength << "  " << _area.bendedLegAngleOffset << "\n"
			<< _area.unbendedLegLength << "  " << _area.unbendedLegAngleOffset << "\nAngles\n"
			<< _area.angle1 << " "  << _area.angle2 << " " << _area.angle3 << " " << _area.angle4 << "\nPoints\n";
		_area.point1.print("p1");
		_area.point2.print("p2");
		_area.point3.print("p3");
		_area.point4.print("p4");
		_area.point5.print("p5");
		_area.point6.print("p6");
		std::cout << "END\n";
		showOnce = false;
	}
}
void LegPositioner::update(double deltaTime) {
	_positioningDurationLeft -= deltaTime;
	if (_pMovement != NULL && ! _pMovement->isFinished()) {
		_pMovement->update(deltaTime);
	}
	else {
		setPositionWithDuration(_targetPositionRelativeToTipCS, deltaTime); // Update current position to apply body movement
	}
}
void LegPositioner::setTipCS(const CoordinateSystem &newTipCS) {
	if (_pMovement != NULL) {
		_pMovement->translate(_tipCS, newTipCS);
	}
	_targetPositionRelativeToTipCS = _tipCS.convertVectorToCS(newTipCS, _targetPositionRelativeToTipCS);
	_tipCS = newTipCS;
}
void LegPositioner::startMovement(MovementBase *pMovement) {
	stopMovement();
	_pMovement = pMovement->copy();
	_pMovement->startWithTarget(this);
}
void LegPositioner::setPositionWithDuration(const Vector &position, double duration) {
	_targetPositionRelativeToTipCS = position;
	Vector newTargetPositionRelativeToShoulderCS = _pShoulderCS->convertVectorFromCS(_tipCS, _targetPositionRelativeToTipCS);
	//position.affineVector().print("Position to be set");
	//_targetPositionRelativeToShoulderCS.affineVector().print("Old position sh");
	//newTargetPositionRelativeToShoulderCS.affineVector().print("Position to be set sh");
	if ((newTargetPositionRelativeToShoulderCS - _targetPositionRelativeToShoulderCS).length() > kDistanceEpsilon) {
		Vector reachablePosition = _getNearestReachablePosition(newTargetPositionRelativeToShoulderCS);
		double servoAngles[3];
		_servoAnglesForTipPosition(reachablePosition, servoAngles);
		_servoControllers[SH].setAngleWithDuration(servoAngles[SH], duration);
		_servoControllers[SV].setAngleWithDuration(servoAngles[SV], duration);
		_servoControllers[KN].setAngleWithDuration(servoAngles[KN], duration);
		//std::cout << "Setting tip's position " << reachablePosition.x << "  " << reachablePosition.y << "  " << reachablePosition.z << "\n";
		_targetPositionRelativeToShoulderCS = newTargetPositionRelativeToShoulderCS;
		_positioningDurationLeft = duration;
	}
}
Vector LegPositioner::_getNearestReachablePosition(const Vector &position) {
	return position;
}
Vector LegPositioner::_getNearestReachablePositionX1Y1(const Vector &positionX1Y1) {
	return positionX1Y1;
}
int LegPositioner::_areaIndexForPositionX1Y1(Vector positionX1Y1) const {
	double direction = positionX1Y1.direction2D();
	if (direction > _area.angle2 && (positionX1Y1 - _area.point6).length() < kTibiaLength) {
		return 1;
	}
	if (direction < _area.angle3 && (positionX1Y1 - _area.point5).length() > kTibiaLength) {
		return 4;
	}
	return 0;
}
void LegPositioner::_servoAnglesForTipPosition(const Vector &position, double *anglesOut) {
	bool shouldInvertX = false;
	Vector positionXY(position.x, position.y);
	double xyDirection = positionXY.direction2D();
	if (positionXY.length() < 0.04) {
		xyDirection *= positionXY.length() / 0.04;
	}
	if (xyDirection > a90) {
		xyDirection -= a180;
		shouldInvertX = true;
	} else if (xyDirection < -a90) {
		xyDirection += a180;
		shouldInvertX = true;
	}
	anglesOut[0] = xyDirection;
	Vector positionX1Y1 = Vector((shouldInvertX? -positionXY.length(): positionXY.length()) - kShoulderLength, position.z);
	positionX1Y1 = _getNearestReachablePositionX1Y1(positionX1Y1);
	_servoAnglesForTipPositionX1Y1(positionX1Y1, anglesOut);
	anglesOut[2] += a90; // Convert CS angle to servo angle
}
void LegPositioner::_servoAnglesForTipPositionX1Y1(const Vector &positionX1Y1, double *anglesOut) {
	Vector pos = positionX1Y1;
	switch (_areaIndexForPositionX1Y1(pos)) {
	case 1:
		{
			pos.x = abs(pos.x);
			if (abs(pos.x) < 0.0001) pos.x = 0.0001;
			float k = pos.y/pos.x;
			float a = (1 + k*k);
			float b = -(2*_area.point6.x + 2*k*_area.point6.y);
			float c = Sqr(_area.point6.x) + Sqr(_area.point6.y) - Sqr(kTibiaLength);
			float D = b*b - 4*a*c;
			float x1 = (-b + sqrt(D)) / (2*a);
			float x2 = (-b - sqrt(D)) / (2*a);
			float x = std::max(x1, x2);
			float y = k*x;
			pos = Vector(x, y);
		}
		break;
	case 4:
		anglesOut[1] = _area.svAngleMin;
		anglesOut[2] = NormalizedAngle((pos - _area.point5).direction2D() - _area.svAngleMin);
		return;
	}
	double distance = std::min(pos.length(), kFemurLength + kTibiaLength);
	double cose = (Sqr(kFemurLength) + Sqr(kTibiaLength) - Sqr(distance))/(2*kFemurLength*kTibiaLength); //cosine theoreme
	cose = Constrain(cose, -1.0, 1.0); // limit cose
	//double kneeAngle = std::acos(cose) - a90;
	double kneeAngle = -(a180 - acos(cose));
	//double shoulderOffsetAngle = -std::acos( Constrain((kFemurLength + kTibiaLength*std::sin(kneeAngle))/distance, -1.0, 1.0) );
	double shoulderOffsetAngle = std::acos( Constrain((kFemurLength + kTibiaLength*std::cos(kneeAngle))/distance, -1.0, 1.0) );
	double shoulderVertAngle = pos.direction2D() + shoulderOffsetAngle;
	anglesOut[1] = shoulderVertAngle;
	anglesOut[2] = kneeAngle;
}



