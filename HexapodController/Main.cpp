#include "HexapodController.h"
#include <XInput.h>

#if defined(HEXAPOD_USE_WEBOTS_SERVOS) || defined(HEXAPOD_USE_WEBOTS_SENSORS)
  #include <webots/Robot.hpp>
  #include <webots/Servo.hpp>
  using webots::Robot;
#else 
	class Robot {
		int step(int deltaTime) {
			return -1;
		}
	};
#endif

namespace Hexapod {
	class HexapodWrapper: public Robot {
		public:
		HexapodWrapper() {
			_totalTime = 0.0;

			DCB dcb;
			_hPort = CreateFileA ("COM5", GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);	
			if ( ! GetCommState(_hPort, &dcb)) {
				std::cout << "!!! Port init error !!! \n";
			}
			// Servos
			PhysicalServoDescriptor psd[6][3] = {
				{{28,750,2250,-93,false,_hPort}, {29,675,2250,-375,false,_hPort}, {30,750,1841,-67,true,_hPort}},
				{{24,750,2250,27,false,_hPort}, {25,1049,2250,-25,false,_hPort}, {26,750,1896,-40,true,_hPort}},
				{{20,650,1900,-375,false,_hPort}, {21,1071,2250,44,false,_hPort}, {22,670,1700,-280,true,_hPort}},
				{{4,750,2250,-20,false,_hPort}, {5,750,2000,60,true,_hPort}, {6,1038,2250,0,false,_hPort}},
				{{8,750,2250,66,false,_hPort}, {9,750,1852,-55,true,_hPort}, {10,1148,2250,80,false,_hPort}},
				{{12,750,2250,44,false,_hPort}, {13,750,1940,-22,true,_hPort}, {14,1170,2250,110,false,_hPort}},
			};
			WebotsServoDescriptor wsd[6][3] = {
				{{"fl_sh", this, -a90, a90}, {"fl_sv", this, -a45, a90}, {"fl_k", this, -a45, a90}},
				{{"ml_sh", this, -a90, a90}, {"ml_sv", this, -a45, a90}, {"ml_k", this, -a45, a90}},
				{{"rl_sh", this, -a90, a90}, {"rl_sv", this, -a45, a90}, {"rl_k", this, -a45, a90}},
				{{"rr_sh", this, -a90, a90}, {"rr_sv", this, -a45, a90}, {"rr_k", this, -a45, a90}},
				{{"mr_sh", this, -a90, a90}, {"mr_sv", this, -a45, a90}, {"mr_k", this, -a45, a90}},
				{{"fr_sh", this, -a90, a90}, {"fr_sv", this, -a45, a90}, {"fr_k", this, -a45, a90}}
			};
			// Touch sensors
			PhysicalSensorDescriptor ptd[6];
			WebotsSensorDescriptor wtd[6] = {
				{this, "fl_ts"}, {this, "ml_ts"}, {this, "rl_ts"}
				, {this, "rr_ts"}, {this, "mr_ts"}, {this, "fr_ts"}
			};
			// Accelerometer
			PhysicalSensorDescriptor pad;
			WebotsSensorDescriptor wad = {this, "acc"};
			_hexapod = new HexapodController(_hPort, psd, wsd, ptd, wtd, &pad, &wad);
		}


		void updateXState() {
			XINPUT_STATE XState;
			ZeroMemory(&XState, sizeof(XState));
			DWORD res = XInputGetState(0, &XState);
			bool _isXConnected = (res == ERROR_SUCCESS);
			if ( _isXConnected )
			{
				// sThumbXX is between -32768 and 32767. Divided by 6000 it lies between 0 and 5, so _xStick.x value is discrete (0.0, 0.2, 0.4 ...)
				static const short kStickDivider = 6000;
				_lstick.x = (XState.Gamepad.sThumbLX / kStickDivider) * 0.2f;
				_lstick.y = (XState.Gamepad.sThumbLY / kStickDivider) * 0.2f;
				_rstick.x = (XState.Gamepad.sThumbRX / kStickDivider) * 0.2f;
				_rstick.y = (XState.Gamepad.sThumbRY / kStickDivider) * 0.2f;
				_buttons = XState.Gamepad.wButtons;
			}
			else
			{
				_lstick.x = _lstick.y = 0.0;
				_rstick.x = _rstick.y = 0.0;
				_buttons = 0;
			}
		}

		void run() {
			while (step(kTimeStep) != -1) {
				updateXState();
				_hexapod->update(kTimeStep/1000.0);
			}

		}

		private:
			HexapodController *_hexapod;
			DWORD _buttons;
			Vector _lstick, _rstick;
			HANDLE _hPort;
			double _totalTime;
	};
}

int main(int argc, char **argv)
{
	Hexapod::HexapodWrapper hc;
	hc.run();
	system("pause");
	return 0;
}