#ifndef __HEXAPOD_LEG_CONTROLLER_H__
#define __HEXAPOD_LEG_CONTROLLER_H__

#include "Generic.h"
#include "GeometricPrimitives.h"
#include "LegPositioner.h"
#include "Sensors.h"

namespace Hexapod {
	enum LegState {
		kLegStateBroken,
		kLegStateMovingBy,
		kLegStateMakingStep,
		kLegStateCannotFinishStep,
		kLegStateSteadyOnGround,
		kLegStateSteadyUnknown,
		kLegStateManualControl
	};
	enum LegStepMode {
		kLegStepModeBlind,
		kLegStepModeTouchAware,
		kLegStepModeDistanceAware
	};
	class LegController {
	public:
		LegController(const CoordinateSystem *pShoulderCS, const CoordinateSystem &tipCS);
		void initPhysicalServos(const PhysicalServoDescriptor *descs) {_positioner.initPhysicalServos(descs);}
		void initWebotsServos(const WebotsServoDescriptor *descs) {_positioner.initWebotsServos(descs);}
		void initPhysicalSensors(const PhysicalSensorDescriptor *pTouchDesc, const PhysicalSensorDescriptor *pDistanceDesc);
		void initWebotsSensors(const WebotsSensorDescriptor *pTouchDesc, const WebotsSensorDescriptor *pDistanceDesc);
		LegStepMode stepMode() const {return _stepMode;}
		void setStepMode(LegStepMode stepMode);
		double stepHeight() const {return _stepHeight;}
		void setStepHeight(double height);
		void makeStep(Vector targetPosition, double duration);
		void moveBy(Vector offset, double duration);
		void update(double deltaTime);
		const CoordinateSystem &tipCS() const {return _positioner.tipCS();}
		void setTipCS(const CoordinateSystem &tipCS) {_positioner.setTipCS(tipCS);}
		void restoreLastPosition();
		void setBroken(bool isBroken);
		LegState state() const {return _state;}
		bool isSteady() const {return _state == kLegStateSteadyOnGround || _state == kLegStateSteadyUnknown;}
		// Manual control
		void startManualControl();
		Vector tipPosition() const {return _positioner.targetPosition();}
		void setTipPosition(Vector position, double duration);
		void stopManualControl();
	private:
		static const int kMaxStepAttemptsCount = 5;
		void _makeBlindStep(Vector targetPosition, double duration);
		void _updateTouchAwareStep(double deltaTime);
		void _makeTouchAwareStep(Vector targetPosition, double duration);
		Vector _targetPositionForTouchAwareStepForAttempt(const Vector &desiredPosition, int attemptInd);
		void _makeDistanceAwareStep(Vector targetPosition, double duration);
		void _updateDistanceAwareStep(double deltaTime);
		LegController(); // = delete
		LegController(const LegController &); // = delete
	private:
		LegPositioner _positioner;
		TouchSensorController _touchSensor;
		DistanceSensorController _distanceSensor;
		Vector _targetStepPosition;
		double _stepDuration;
		Vector _lastSteadyPosition;
		LegState _state;
		LegStepMode _stepMode;
		double _stepHeight;
		int _stepAttemptInd;
	};
}

#endif