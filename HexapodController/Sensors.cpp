#include "Sensors.h"
using namespace Hexapod;

TouchSensorController::TouchSensorController() {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	_webotsSensor = NULL;
#endif
#ifdef HEXAPOD_USE_REAL_SENSORS
#endif
}
void TouchSensorController::initWebotsSensor(const WebotsSensorDescriptor *pDesc) {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	_webotsSensor = ((webots::Robot*)(pDesc->robot))->getTouchSensor(pDesc->name);
	_webotsSensor->enable(kTimeStep);
#endif
}
void TouchSensorController::initPhysicalSensor(const PhysicalSensorDescriptor *pDesc) {
#ifdef HEXAPOD_USE_REAL_SENSORS
#endif
}
bool TouchSensorController::isPressed() const {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	assert(_webotsSensor != NULL);
	return (_webotsSensor->getValue() > 50.0);
#elif defined(HEXAPOD_USE_REAL_SENSORS)
	return false;
#else
	return false;
#endif
}

DistanceSensorController::DistanceSensorController() {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	_webotsSensor = NULL;
#endif
#ifdef HEXAPOD_USE_REAL_SENSORS
#endif
}
void DistanceSensorController::initWebotsSensor(const WebotsSensorDescriptor *pDesc) {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	_webotsSensor = ((webots::Robot*)(pDesc->robot))->getDistanceSensor(pDesc->name);
	_webotsSensor->enable(kTimeStep);
#endif
}
void DistanceSensorController::initPhysicalSensor(const PhysicalSensorDescriptor *pDesc) {
#ifdef HEXAPOD_USE_REAL_SENSORS
#endif
}
double DistanceSensorController::distance() const {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	assert(_webotsSensor != NULL);
	return _webotsSensor->getValue();
#elif defined(HEXAPOD_USE_REAL_SENSORS)
	return 1.0;
#else
	return 1.0;
#endif
}

AccelerometerController::AccelerometerController()
	: _smoothedValue(0.0, 0.0, 9.82) {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	_webotsSensor = NULL;
#endif
#ifdef HEXAPOD_USE_REAL_SENSORS
#endif
}
void AccelerometerController::initWebotsAccelerometer(const WebotsSensorDescriptor *pDesc) {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	_webotsSensor = ((webots::Robot*)(pDesc->robot))->getAccelerometer(pDesc->name);
	_webotsSensor->enable(kTimeStep);
#endif
}
void AccelerometerController::initPhysicalAccelerometer(const PhysicalSensorDescriptor *pDesc) {
#ifdef HEXAPOD_USE_REAL_SENSORS
#endif
}
Vector AccelerometerController::value() const {
	return _smoothedValue;
}
Vector AccelerometerController::_primitiveValue() const {
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
	assert(_webotsSensor != NULL);
	return Vector(_webotsSensor->getValues()[0], -_webotsSensor->getValues()[2], _webotsSensor->getValues()[1]);
#elif defined(HEXAPOD_USE_REAL_SENSORS)
	return Vector(0.0, 0.0, 9.82);
#else
	return Vector(0.0, 0.0, 9.82);
#endif
}
void AccelerometerController::update(double deltaTime) {
	static int c = 0;
	deltaTime = Constrain(deltaTime, 0.0, 0.1);
	_smoothedValue = _primitiveValue() * deltaTime + _smoothedValue * (1.0 - deltaTime);
	//if (c % 100 == 0)_smoothedValue.affineVector().print("Acc");
	++c;
}