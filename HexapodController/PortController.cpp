#include "PortController.h"
using namespace Hexapod;

#if defined(HEXAPOD_USE_REAL_SERVOS) || defined(HEXAPOD_USE_REAL_SENSORS)

std::map<HANDLE, PortController*> PortController::_portControllers;

PortController *PortController::GetControllerForPortHandle(HANDLE hPort) {
	PortController *res;
	std::map<HANDLE, PortController*>::iterator resIter = _portControllers.find(hPort);
	if (resIter == _portControllers.end()) {
		res = new PortController(hPort);
		_portControllers[hPort] = res;
	}
	else {
		res = resIter->second;
	}
	return res;
}

void PortController::ClearControllers() {
	std::map<HANDLE, PortController*>::iterator iter = _portControllers.begin();
	while (iter != _portControllers.end()) {
		delete iter->second;
		_portControllers.erase(iter++);
	}
}

PortController::PortController(HANDLE hPort)
	: _hPort(hPort), _ready(true) {
	*_messageBuffer = '\0';
}

bool PortController::flush() {
	if (_messageBuffer[0] != '\0') {
		Print(_messageBuffer, kVerbosityHigh);
		DWORD bytesWritten = 0;
		DWORD bytesToWrite = std::strlen(_messageBuffer);
		bool writeOk = WriteFile(_hPort, _messageBuffer, bytesToWrite, &bytesWritten, NULL);
		if ( ! writeOk || bytesWritten != bytesToWrite) {
			_ready = false;
			Warn("Sending message error:");
			Warn(_messageBuffer);
		}
		_messageBuffer[0] = '\0';
		return _ready;
	}
	else {
		Print("Trying to transmit empty message", kVerbosityMedium);
		return true;
	}
}
#endif