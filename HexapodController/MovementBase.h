#ifndef __HEXAPOD_MOVEMENT_BASE_H__
#define __HEXAPOD_MOVEMENT_BASE_H__

#include "Generic.h"
#include "MovementPath.h"
#include "GeometricPrimitives.h"

namespace Hexapod {
	class Movable {
	public:
		virtual Vector targetPosition() const = 0;
		virtual void setPositionWithDuration(const Vector &position, double duration) = 0;
	};

	class MovementBase {
	public:
		MovementBase(): _isFinished(false), _pTarget(NULL) {}
		virtual void startWithTarget(Movable *pTarget) {_pTarget = pTarget; _isFinished = false;}
		Movable *target() {return _pTarget;}
		virtual void update(double deltaTime) {assert(_pTarget != NULL);}
		bool isFinished() const {return _isFinished;}
		virtual void translate(const CoordinateSystem &fromCS, const CoordinateSystem &toCS) {}
		virtual MovementBase *copy() const = 0;
	protected:
		Movable *_pTarget;
		bool _isFinished;
	};
}

#endif