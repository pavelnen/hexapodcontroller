#ifndef __HEXAPOD_PORT_CONTROLLER_H__
#define __HEXAPOD_PORT_CONTROLLER_H__

#include "Generic.h"
#include "HexapodConfig.h"
#include <map>

namespace Hexapod {
#if defined(HEXAPOD_USE_REAL_SERVOS) || defined(HEXAPOD_USE_REAL_SENSORS)
	class PortController {
	public:
		static PortController *GetControllerForPortHandle(HANDLE hPort);
		static void ClearControllers();
		HANDLE getHandle() const {return _hPort;}
		bool isReady() const {return _ready;}
		bool isEmpty() const {return _messageBuffer[0] == '\0';}
		const char *getMessage() const {return _messageBuffer;}
		void setMessage(const char *msg) {strcpy(_messageBuffer, msg);}
		void appendMessage(const char *msg) {strcat(_messageBuffer, msg);}
		bool flush();
	private:
		PortController(HANDLE hPort);
	private:
		static std::map<HANDLE, PortController *> _portControllers;
		HANDLE _hPort;
		char _messageBuffer[2048];
		bool _ready;
	};
#endif
}

#endif