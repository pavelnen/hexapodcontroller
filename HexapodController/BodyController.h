#ifndef __HEXAPOD_BODY_CONTROLLER_H__
#define __HEXAPOD_BODY_CONTROLLER_H__

#include "Generic.h"
#include "HexapodConfig.h"
#include "GeometricPrimitives.h"
#include "LegController.h"

namespace Hexapod {
	class BodyController {
	public:
		BodyController();
		BodyController(const CoordinateSystem &bodyCS);
		void setPosition(Vector position) {_position = position;}
		Vector position() const {return _position;}
		const CoordinateSystem &cs() const {return _bodyCS;}
		const CoordinateSystem *shoulderCS(LegIndex leg) {return &_shouldersCS[leg];}
		void setLegControllers(LegController *const*legsControllers) {_legsControllers = legsControllers;}
		//void alignByLegs(bool *legsToAlignBy); // 6 bools mask
		void alignBy(const Vector &rotationVector, const Vector &legsCenter);
		Vector averageLegsPositionRelativeToBody(bool *activeLegs) const;
		Vector rotationVectorForLegsPositions(Vector *legsPositions, bool *activeLegs) const;
	private:
		void _initShouldersCS();
		double _rotationAngleForLegsPositions(Vector *legsPositions, bool *activeLegs, bool rotateByX) const;
	private:
		Vector _position;
		CoordinateSystem _bodyCS;
		CoordinateSystem _shouldersCS[kLegsCount];
		const LegController *const*_legsControllers;
	};
}

#endif

