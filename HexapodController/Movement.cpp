#include "Movement.h"
using namespace Hexapod;

MoveBy::MoveBy(const PathBase *pPath, double duration, bool shouldNormalizeTime, const arma::mat44 &affineTransformation)
	: _duration(duration), _shouldNormalizeTime(shouldNormalizeTime), _pPath(pPath->copy()), _affineTransformation(affineTransformation)
{
}
void MoveBy::startWithTarget(Movable *pTarget) {
	MovementBase::startWithTarget(pTarget);
	_time = 0.0;
	_initPosition = _pTarget->targetPosition();
}
void MoveBy::update(double deltaTime) {
	MovementBase::update(deltaTime);
	if ( ! _isFinished) {
		double oldTime = _time;
		_time += deltaTime;
		if (_time > _duration) {
			_isFinished = true;
			_time = _duration;
		}
		deltaTime = _time - oldTime;
		Vector offset = _pPath->positionAtTime(_shouldNormalizeTime? (_duration > 0.0? (_time/_duration): 1.0): _time);
		//std::cout << "Offset    " << offset.x << "  " << offset.y << "  " << offset.z << "\n";
		Vector offsetTransformed = Vector(_affineTransformation * offset.affineVector());
		//std::cout << "Offset tr " << offsetTransformed.x << "  " << offsetTransformed.y << "  " << offsetTransformed.z << "\n";
		Vector newPosition = _initPosition + offsetTransformed;
		_pTarget->setPositionWithDuration(newPosition, deltaTime);
	}
}
void MoveBy::translate(const CoordinateSystem &fromCS, const CoordinateSystem &toCS) {
	MovementBase::translate(fromCS, toCS);
	//_initPosition = fromCS.convertVectorToCS(toCS, _initPosition);
	_affineTransformation = fromCS.convertAffineToCs(toCS, _affineTransformation);
}


MovementDelay::MovementDelay(double duration)
	: _duration(duration) {
}
void MovementDelay::startWithTarget(Movable *pTarget) {
	MovementBase::startWithTarget(pTarget);
	_time = 0.0;
}
void MovementDelay::update(double deltaTime) {
	MovementBase::update(deltaTime);
	if ( ! _isFinished) {
		_time += deltaTime;
		_isFinished = (_time >= _duration);
	}
}


MovementSequance::~MovementSequance() {
	for (_currentMovement = _movements.begin(); _currentMovement != _movements.end(); ++_currentMovement) {
		delete (*_currentMovement);
	}
}
void MovementSequance::startWithTarget(Movable *pTarget) {
	MovementBase::startWithTarget(pTarget);
	_currentMovement = _movements.begin();
	_isFinished = (_currentMovement == _movements.end());
	if ( ! _isFinished) {
		(*_currentMovement)->startWithTarget(pTarget);
	}
}
void MovementSequance::update(double deltaTime) {
	MovementBase::update(deltaTime);
	if ( ! _isFinished) {
		(*_currentMovement)->update(deltaTime);
		while (_currentMovement != _movements.end() && (*_currentMovement)->isFinished()) {
			++_currentMovement;
			if (_currentMovement == _movements.end()) {
				_isFinished = true;
			}
			else {
				(*_currentMovement)->startWithTarget(_pTarget);
			}
		}
	}
}
MovementBase *MovementSequance::copy() const {
	MovementSequance *newInstance = new MovementSequance();
	for (std::list<MovementBase*>::const_iterator iter = _movements.begin(); iter != _movements.end(); ++iter) {
		newInstance->addMovement(*iter);
	}
	return newInstance;
}
void MovementSequance::translate(const CoordinateSystem &fromCS, const CoordinateSystem &toCS) {
	MovementBase::translate(fromCS, toCS);
	if ( ! _isFinished) {
		(*_currentMovement)->translate(fromCS, toCS);
	}
}


MoveTillPressed::MoveTillPressed(const MovementBase *pMovement, const TouchSensorController *pTouchSensor, double samplingInterval)
	: _pMovement(pMovement->copy()), _pTouchSensor(pTouchSensor), _samplingInterval(samplingInterval) {
}
void MoveTillPressed::startWithTarget(Movable *pTarget) {
	MovementBase::startWithTarget(pTarget);
	_pMovement->startWithTarget(pTarget);
	_isFinished = _pMovement->isFinished();
}
void MoveTillPressed::update(double deltaTime) {
	MovementBase::update(deltaTime);
	if ( ! _isFinished ) {
		if (_pMovement->isFinished() || _pTouchSensor->isPressed()) {
			_isFinished = true;
		}
		else {
			_pMovement->update(deltaTime);
		}
	}
}