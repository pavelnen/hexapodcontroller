#include "BodyController.h"
using namespace Hexapod;

BodyController::BodyController()
	:_bodyCS(kCentralCS.affineMatrix(), &kGlobalCS)
	, _position(0.0, 0.0, 0.0) {
		_initShouldersCS();
}
BodyController::BodyController(const CoordinateSystem &bodyCS)
	:_bodyCS(bodyCS)
	, _position(0.0, 0.0, 0.0) {
		_initShouldersCS();
}
void BodyController::_initShouldersCS() {
	for (LegIndex leg = 0; leg < kLegsCount; ++leg) {
		double xPos = std::cos(kLegsDirections[leg]) * kBodyRadius;
		double yPos = std::sin(kLegsDirections[leg]) * kBodyRadius;
		double zPos = 0.0;//kBodyCenterToShoulderVerticalOffset;
		Vector xAxis(std::cos(kLegsDirections[leg]), std::sin(kLegsDirections[leg]), 0.0);
		Vector yAxis(-std::sin(kLegsDirections[leg]), std::cos(kLegsDirections[leg]), 0.0);
		Vector zAxis(0.0, 0.0, 1.0);
		_shouldersCS[leg] = CoordinateSystem(xAxis, yAxis, zAxis, Vector(xPos, yPos, zPos), &_bodyCS);
	}
}
/*
void BodyController::alignByLegs(bool *legsToAlignBy) {
	Vector legsPositions[kLegsCount];
	for (LegIndex leg = 0; leg < kLegsCount; ++leg) {
		if (legsToAlignBy[leg]) {
			legsPositions[leg] = _bodyCS.parentCS()->convertVectorFromCS(_legsControllers[leg]->tipCS(), _legsControllers[leg]->tipPosition());
		}
	}
	_rotationVectorForLegsPositions(legsPositions, legsToAlignBy).affineVector().print("Rotation vector");
	double angleX = _rotationAngleForLegsPositions(legsPositions, legsToAlignBy, true);
	CoordinateSystem tempBodyCS(
		  Vector(1.0, 0.0, 0.0)
		, Vector(0.0, std::cos(angleX), std::sin(angleX))
		, Vector(0.0, -std::sin(angleX), std::cos(angleX))
		, Vector(0.0), _bodyCS.parentCS());
	//std::cout << angleX << "   ";
	//setPosition(newBodyCS);

	// Rotate body about Y axis
	for (LegIndex leg = 0; leg < kLegsCount; ++leg) { // Find position of legs in new coordinate system
		if (legsToAlignBy[leg]) {
			legsPositions[leg] = tempBodyCS.convertVectorFromCS(_legsControllers[leg]->tipCS(), _legsControllers[leg]->tipPosition());
		}
	}
	double angleY = _rotationAngleForLegsPositions(legsPositions, legsToAlignBy, false);
	//std::cout << angleY << "\n";
	double cose = std::cos(angleY);
	double one_cose = 1.0 - cose;
	double sine = -std::sin(angleY);
	Vector n = tempBodyCS.yAxis(); double nx=n.x, ny=n.y, nz = n.z;
	CoordinateSystem rotationCS( // Rotation about vector n (wiki), can be optimized (nx is always 0)
		  Vector(cose+one_cose*nx*nx, one_cose*nx*ny-sine*nx, one_cose*nx*nz+sine*ny)
		, Vector(one_cose*ny*nx+sine*nz, cose+one_cose*ny*ny, one_cose*ny*ny-sine*nx)
		, Vector(one_cose*nx*nz-sine*ny, one_cose*nz*ny+sine*nx, cose+one_cose*nz*nz)
		, Vector(0.0), NULL
		);
	Vector xAxisRotatedAboutY = rotationCS*tempBodyCS.xAxis();
	Vector zAxisRotatedAboutY = rotationCS*tempBodyCS.zAxis();
	CoordinateSystem newBodyCS(xAxisRotatedAboutY, tempBodyCS.yAxis(), zAxisRotatedAboutY, Vector(0.0), _bodyCS.parentCS());
	_bodyCS = newBodyCS;
}*/
/* void BodyController::alignByLegs(bool *legsToAlignBy) {
	_position.affineVector().print("Bpos");
	Vector legsPositions[kLegsCount];
	for (LegIndex leg = 0; leg < kLegsCount; ++leg) {
		if (legsToAlignBy[leg]) {
			legsPositions[leg] = _bodyCS.parentCS()->convertVectorFromCS(_legsControllers[leg]->tipCS(), _legsControllers[leg]->tipPosition());
		}
	}
	Vector rotationVector = rotationVectorForLegsPositions(legsPositions, legsToAlignBy);
	double angle = 1.7 * rotationVector.length();
	double cose = std::cos(angle);
	double one_cose = 1.0 - cose;
	double sine = std::sin(angle);
	Vector legsCenter = averageLegsPositionRelativeToBody(legsToAlignBy);
	Vector desiredLegsCenter = _position + Vector(0.0, 0.0, -kTibiaLength);
	Vector deltaLegsCenterRelativeToBody = legsCenter - desiredLegsCenter;
	Vector deltaLegsCenterRalativeToGlobal = _bodyCS.parentCS()->convertVectorFromCS(_bodyCS, deltaLegsCenterRelativeToBody);
	if (angle > a180 / 1800) { // 0.1 degree
		Vector n = rotationVector.normalized(); double nx=n.x, ny=n.y, nz = n.z;
		CoordinateSystem newBodyCS(
			Vector(cose+one_cose*nx*nx, one_cose*ny*nx+sine*nz, one_cose*nx*nz-sine*ny)
			, Vector(one_cose*nx*ny-sine*nx, cose+one_cose*ny*ny, one_cose*nz*ny+sine*nx)
			, Vector(one_cose*nx*nz+sine*ny, one_cose*ny*ny-sine*nx, cose+one_cose*nz*nz)
			, _position + deltaLegsCenterRalativeToGlobal, _bodyCS.parentCS()
			);
		_bodyCS = newBodyCS;
	}
	else {
		_bodyCS = CoordinateSystem(_position + deltaLegsCenterRalativeToGlobal, _bodyCS.parentCS());
	}
} */
void BodyController::alignBy(const Vector &rotationVector, const Vector &legsCenter) {
	//_position.affineVector().print("Bpos");
	double angle = 1.7 * rotationVector.length();
	Vector desiredLegsCenter = Vector(0.0, 0.0, -kBodyHeight) - _position;
	Vector deltaLegsCenter = legsCenter - desiredLegsCenter;
	//deltaLegsCenter.affineVector().print("Delta");
	if (angle > a180 / 1800) { // 0.1 degree
		double cose = std::cos(angle);
		double one_cose = 1.0 - cose;
		double sine = std::sin(angle);
		Vector n = rotationVector.normalized(); double nx=n.x, ny=n.y, nz = n.z;
		CoordinateSystem newBodyCS(
			Vector(cose+one_cose*nx*nx, one_cose*ny*nx+sine*nz, one_cose*nx*nz-sine*ny)
			, Vector(one_cose*nx*ny-sine*nx, cose+one_cose*ny*ny, one_cose*nz*ny+sine*nx)
			, Vector(one_cose*nx*nz+sine*ny, one_cose*ny*ny-sine*nx, cose+one_cose*nz*nz)
			, deltaLegsCenter, _bodyCS.parentCS()
			);
		//static int c = 0; if ((++c)%50 == 1) newBodyCS.affineMatrix().print("BCS");
		_bodyCS = newBodyCS;
	}
	else {
		_bodyCS = CoordinateSystem(deltaLegsCenter, _bodyCS.parentCS());
	}
}

Vector BodyController::rotationVectorForLegsPositions(Vector *legsPositions, bool *activeLegsMask) const {
	//Vector legsCenter(0.0);
	Vector rotationCenter(0.0, 0.0, -kBodyHeight);
	int activeLegsCount = 0;

	Vector rotationVectorSum(0.0);
	double rotationWeightSum = 0.001;
	for (LegIndex leg = 0; leg < kLegsCount; ++leg) {
		if (activeLegsMask[leg]) {
			activeLegsCount += 1;
			Vector delta = legsPositions[leg] - rotationCenter;
			Vector defaultLegPosition(kTipRadialDistance * std::cos(kLegsDirections[leg]), kTipRadialDistance * std::sin(kLegsDirections[leg]), -kBodyHeight);
			Vector defaultDelta = defaultLegPosition - rotationCenter;
			Vector rotationVector = defaultDelta.cross(delta) / (delta.length() + 0.001) / (defaultDelta.length() + 0.001); // Add 1mm to avoid division by zero
			double rotationWeight = delta.length();
			rotationWeightSum += delta.length();
			rotationVectorSum += rotationVector * rotationWeight;
		}
	}
	if (activeLegsCount > 0) {
		return rotationVectorSum / rotationWeightSum;
	}
	else {
		return Vector(0.0, 0.0, 0.0);
	}
}
double BodyController::_rotationAngleForLegsPositions(Vector *legsPositions, bool *activeLegs, bool rotateByX) const {
	Vector legsCenter(0.0);
	int activeLegsCount = 0;
	for (LegIndex leg = 0; leg < kLegsCount; ++leg) {
		legsCenter += legsPositions[leg];
		++activeLegsCount;
	}
	if (activeLegsCount > 0) {
		legsCenter /= activeLegsCount;
		double ratioMin = 10.0;
		double ratioMinWeight = 1.0;
		double leveredRatioMin = 10.0;
		double ratioMax = -10.0;
		double ratioMaxWeight = 1.0;
		double leveredRatioMax = -10.0;
		for (LegIndex leg = 0; leg < kLegsCount; ++leg) {
			if (activeLegs[leg]) {
				Vector delta = legsPositions[leg] - legsCenter;
				double lever = rotateByX? delta.y: delta.x;
				double leveredRatio = delta.z * lever;
				double ratio = delta.z / lever;
				if (leveredRatio < leveredRatioMin) {
					leveredRatioMin = leveredRatio;
					ratioMin = ratio;
					ratioMinWeight = std::abs(lever);
				}
				if (leveredRatio > leveredRatioMax) {
					leveredRatioMax = leveredRatio;
					ratioMax = ratio;
					ratioMaxWeight = std::abs(lever);
				}
			}
		}
		double averageRatio = (ratioMin*ratioMinWeight + ratioMax*ratioMaxWeight) / (ratioMinWeight + ratioMaxWeight);
		return std::atan(averageRatio);
	}
	else {
		return 0;
	}
}
Vector BodyController::averageLegsPositionRelativeToBody(bool *activeLegs) const {
	Vector legCenter(0.0);
	int activeLegsCount = 0;
	for (LegIndex leg = 0; leg <= kLastLeg; ++leg) {
		if (activeLegs[leg]) {
			legCenter += _bodyCS.convertVectorFromCS(_legsControllers[leg]->tipCS(), _legsControllers[leg]->tipPosition());
			++activeLegsCount;
		}
	}
	if (activeLegsCount > 0) {
		legCenter /= activeLegsCount;
		return legCenter;
	}
	else {
		return _position;
	}
}