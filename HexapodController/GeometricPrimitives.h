#ifndef __HEXAPOD_GEOMETRIC_PRIMITIVES_H__
#define __HEXAPOD_GEOMETRIC_PRIMITIVES_H__
#include "Generic.h"
#include "lib\armadillo"

namespace Hexapod {
	struct Vector {
		double x, y, z;
		explicit Vector(double x = 0.0, double y = 0.0, double z = 0.0):x(x), y(y), z(z) {}
		Vector(const arma::vec3 &vector, bool b): x(vector(0)), y(vector(1)), z(vector(2)) {}
		Vector(const arma::vec4 &affineVector): x(affineVector[0]), y(affineVector[1]), z(affineVector[2])  {}
		arma::vec4 affineVector() const;
		Vector fromPolar() const;
		double length() const;
		double direction2D() const;
		Vector normalized() const;
		double distance(const Vector &v) const;
		double dot(const Vector &v) const;
		Vector cross(const Vector &v) const;
		Vector operator-();
		Vector &operator+=(const Vector &v);
		Vector &operator-=(const Vector &v);
		Vector &operator*=(double s);
		Vector &operator/=(double s);
		friend Vector operator+(const Vector &v1, const Vector &v2);
		friend Vector operator-(const Vector &v1, const Vector &v2);
		friend Vector operator*(const Vector &v1, const Vector &v2);
		friend Vector operator/(const Vector &v1, const Vector &v2);
		friend Vector operator*(const Vector &v, double s);
		friend Vector operator/(const Vector &v, double s);
		void print(const char *prefix = "", const char *suffix = "\n");
	};

	typedef arma::mat44 Affine;
	Vector GetAffineOffset(const Affine &affine);
	void SetAffineOffset(Affine *pAffine, const Vector &offset);

	class CoordinateSystem {
		const CoordinateSystem *_parent;
		Affine _affine;
	public:
		CoordinateSystem();
		explicit CoordinateSystem(const CoordinateSystem *pParent);
		CoordinateSystem(Vector offset, const CoordinateSystem *pParent);
		CoordinateSystem(Vector xAxis, Vector yAxis, Vector zAxis, Vector offset, const CoordinateSystem *pParent);
		CoordinateSystem(const arma::mat44 &affineMatrix, const CoordinateSystem *parent);
		const Affine &affineMatrix() const;
		Vector xAxis() const;
		Vector yAxis() const;
		Vector zAxis() const;
		Vector offset() const;
		void setOffset(Vector offset);
		CoordinateSystem csRelativeToGlobalCS() const;
		Vector convertVectorFromCS(const CoordinateSystem &fromCS, const Vector &vector) const;
		Vector convertVectorToCS(const CoordinateSystem &toCS, const Vector &vector) const;
		Affine convertAffineFromCs(const CoordinateSystem &fromCS, const Affine &affine) const;
		Affine convertAffineToCs(const CoordinateSystem &toCS, const Affine &affine) const;
		const CoordinateSystem *parentCS() const {return _parent;}
		void setParentCS(const CoordinateSystem *newParentCS);
		CoordinateSystem inversed() const;
		friend CoordinateSystem operator*(const CoordinateSystem &cs1, const CoordinateSystem &cs2);
		friend Vector operator*(const CoordinateSystem &cs, const Vector &v);
		static CoordinateSystem rotationCS(Vector rotationVector);
	};

//	double AngleForDirection2D(const Vector &positionXY); // Z is not utilized
	extern const CoordinateSystem kGlobalCS;
	extern const CoordinateSystem kCentralCS;
}

#endif