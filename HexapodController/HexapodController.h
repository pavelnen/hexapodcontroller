#ifndef __HEXAPOD_HEXAPOD_CONTROLLER_H__
#define __HEXAPOD_HEXAPOD_CONTROLLER_H__

#include "Generic.h"
#include "GeometricPrimitives.h"
#include "LegController.h"
#include "BodyController.h"
#include "PortController.h"

namespace Hexapod {
	class HexapodController {
	public:
		HexapodController(HANDLE hPort
			, PhysicalServoDescriptor ppsd[][3], WebotsServoDescriptor pwsd[][3]
			, PhysicalSensorDescriptor *pptd, WebotsSensorDescriptor *pwtd
			, PhysicalSensorDescriptor *ppad, WebotsSensorDescriptor *pwad);
		void setMovementSpeed(const Vector &speed) {_movementSpeed = speed;}
		Vector movementSpeed() const {return _movementSpeed;}
		void setBodyHeight(double height) {_bodyHeight = height;}
		double bodyHeight() const {return _bodyHeight;}
		void setRotationSpeed(double rotationSpeed) {_rotationSpeed = rotationSpeed;}
		double rotationSpeed() const {return _rotationSpeed;}
		void startManualControl(LegIndex leg) {_legs[leg]->startManualControl();}
		Vector legPosition(LegIndex leg) const {return _legs[leg]->tipPosition();}
		void setLegPositionWithDuration(LegIndex leg, const Vector &position, double duration);
		void stopManualControlAll();
		void update(double deltaTime);
	private:
		Vector _rotationVectorForLegsPositions(const Vector *legsPositions, const bool *activeLegs
																	, const Vector &rotationCenter);
		Vector _legsCenterForLegsPositions(const Vector *legsPositions, const bool *activeLegs);
		void _recoveryDeltasRelativeToTip(const bool *activeLegs, Vector *deltasOut);
	private:
		BodyController *_body;
		LegController *_legs[kLegsCount];
		AccelerometerController _accelerometer;
		HANDLE _hPort;
		Vector _movementSpeed;
		double _rotationSpeed;
		double _bodyHeight;
		int _duopodInd;
	};
}

#endif