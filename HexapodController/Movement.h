#ifndef __HEXAPOD_MOVEMENT_H__
#define __HEXAPOD_MOVEMENT_H__

#include "Generic.h"
#include "MovementBase.h"
#include "Sensors.h"
#include <list>

namespace Hexapod {

	class MoveBy: public MovementBase {
	public:
		MoveBy(const PathBase *pPath, double duration, bool shouldNormalizeTime, const arma::mat44 &affineTransformation = kCentralCS.affineMatrix());
		~MoveBy() {SAFE_DELETE(_pPath);}
		virtual void startWithTarget(Movable *pTarget);
		virtual void update(double deltaTime);
		virtual void translate(const CoordinateSystem &fromCS, const CoordinateSystem &toCS);
		virtual MovementBase *copy() const {return new MoveBy(_pPath, _duration, _shouldNormalizeTime, _affineTransformation);}
		const PathBase *path() const {return _pPath;}
		double duration() const {return _duration;}
		bool shouldNormalizeTime() const {return _shouldNormalizeTime;}
		const Affine &affine() const {return _affineTransformation;}
		void setAffine(const Affine &affine) {_affineTransformation = affine;}
	private:
		MoveBy(); // = delete
		MoveBy(const MoveBy &); // = delete
	private:
		double _time;
		double _duration;
		bool _shouldNormalizeTime;
		Vector _initPosition;
		const PathBase *_pPath;
		Affine _affineTransformation;

	};

	class MovementDelay: public MovementBase {
	public:
		MovementDelay(double duration);
		virtual void startWithTarget(Movable *pTarget);
		virtual void update(double deltaTime);
		virtual MovementBase *copy() const {return new MovementDelay(*this);}
	protected:
		double _time;
		double _duration;
	};

	class MovementSequance: public MovementBase {
	public:
		MovementSequance() {}
		~MovementSequance();
		void  addMovement(const MovementBase *movement) {_movements.push_back(movement->copy());}
		virtual void startWithTarget(Movable *pTarget);
		virtual void update(double deltaTime);
		virtual void translate(const CoordinateSystem &fromCS, const CoordinateSystem &toCS);
		virtual MovementBase *copy() const;
	protected:
		std::list<MovementBase*> _movements;
		std::list<MovementBase*>::iterator _currentMovement;
	};

	class MoveTillPressed: public MovementBase {
	public:
		// TODO : sampling interval
		MoveTillPressed(const MovementBase *pMovement, const TouchSensorController *pTouchSensor, double samplingInterval = 0.0);
		~MoveTillPressed() {SAFE_DELETE(_pMovement);}
		virtual void startWithTarget(Movable *pTarget);
		virtual void update(double deltaTime);
		virtual void translate(const CoordinateSystem &fromCS, const CoordinateSystem &toCS) {_pMovement->translate(fromCS, toCS);}
		virtual MovementBase *copy() const {return new MoveTillPressed(_pMovement, _pTouchSensor, _samplingInterval);}
	private:
		MoveTillPressed(); // = delete
		MoveTillPressed(const MoveTillPressed &); // = delete
	private:
		MovementBase *_pMovement;
		double _samplingInterval;
		const TouchSensorController *_pTouchSensor;
	};
}

#endif