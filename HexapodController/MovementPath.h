#ifndef __HEXAPOD_MOVEMENT_PATH_H__
#define __HEXAPOD_MOVEMENT_PATH_H__

#include "Generic.h"
#include "GeometricPrimitives.h"

namespace Hexapod {
	class PathBase {
	public:
		virtual PathBase *copy() const = 0;
		virtual Vector positionAtTime(double time) const = 0;
	};

	class LinearPath: public PathBase {
	public:
		static const LinearPath *shared() {return &_instance;}
		LinearPath(Vector offset): _offset(offset) {}
		virtual Vector positionAtTime(double time) const;
		virtual PathBase *copy() const {return new LinearPath(_offset);}
	private:
		static LinearPath _instance;
		LinearPath() {}
		LinearPath(const LinearPath &lp); // = delete;
		Vector _offset;
	};

	class ArcPathZ:public PathBase {
	public:
		static const ArcPathZ *shared() {return &_instance;}
		ArcPathZ(Vector offset): _offset(offset) {}
		virtual Vector positionAtTime(double time) const;
		virtual PathBase *copy() const {return new ArcPathZ(_offset);}
	private:
		static ArcPathZ _instance;
		Vector _offset;
	};

	class StepPath: public PathBase {
	public:
		StepPath(Vector offset, double height);
		virtual Vector positionAtTime(double time) const;
		virtual PathBase *copy() const {return new StepPath(_offset, _height);}
	private:
		ArcPathZ _phase1;
		ArcPathZ _phase2;
		Vector _offset;
		Vector _midPosition;
		double _height;
	};
}

#endif