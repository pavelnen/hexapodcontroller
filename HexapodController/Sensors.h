#ifndef __HEXAPOD_SENSORS_H__
#define __HEXAPOD_SENSORS_H__

#include "Generic.h"
#include "HexapodConfig.h"
#include "GeometricPrimitives.h"
#ifdef HEXAPOD_USE_WEBOTS_SERVOS
  #include <webots/Robot.hpp>
  #include <webots/Servo.hpp>
#endif

#include <webots/TouchSensor.hpp>
#include <webots/DistanceSensor.hpp>
#include <webots/Accelerometer.hpp>


namespace Hexapod {
	struct WebotsSensorDescriptor {
		void *robot;
		const char *name;
	};

	struct PhysicalSensorDescriptor { };

	class TouchSensorController {
	public:
		TouchSensorController();
		void initWebotsSensor(const WebotsSensorDescriptor *pDesc);
		void initPhysicalSensor(const PhysicalSensorDescriptor *pDesc);
		bool isPressed() const;
	private:
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
		webots::TouchSensor *_webotsSensor;
#endif
	};

	class DistanceSensorController {
	public:
		DistanceSensorController();
		void initWebotsSensor(const WebotsSensorDescriptor *pDesc);
		void initPhysicalSensor(const PhysicalSensorDescriptor *pDesc);
		double distance() const;
	private:
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
		webots::DistanceSensor *_webotsSensor;
#endif
	};

	class AccelerometerController {
	public:
		AccelerometerController();
		void initWebotsAccelerometer(const WebotsSensorDescriptor *pDesc);
		void initPhysicalAccelerometer(const PhysicalSensorDescriptor *pDesc);
		void update(double deltaTime);
		Vector value() const;
	private:
		Vector _primitiveValue() const;
		Vector _smoothedValue;
#ifdef HEXAPOD_USE_WEBOTS_SENSORS
		webots::Accelerometer *_webotsSensor;
#endif
	};
}

#endif