#include "LegController.h"
#include "Movement.h"
using namespace Hexapod;

LegController::LegController(const CoordinateSystem *pShoulderCS, const CoordinateSystem &tipCS)
	:_positioner(pShoulderCS, tipCS)
	, _targetStepPosition(0.0)
	, _lastSteadyPosition(0.0)
	, _stepHeight(0.05)
	, _stepMode(kLegStepModeBlind)
	, _state(kLegStateSteadyUnknown) {
}
void LegController::initPhysicalSensors(const PhysicalSensorDescriptor *pTouchDesc, const PhysicalSensorDescriptor *pDistanceDesc) {
	if (pTouchDesc != NULL) _touchSensor.initPhysicalSensor(pTouchDesc);
	if (pDistanceDesc != NULL) _distanceSensor.initPhysicalSensor(pDistanceDesc);
}
void LegController::initWebotsSensors(const WebotsSensorDescriptor *pTouchDesc, const WebotsSensorDescriptor *pDistanceDesc) {
	if (pTouchDesc != NULL) _touchSensor.initWebotsSensor(pTouchDesc);
	if (pDistanceDesc != NULL) _distanceSensor.initWebotsSensor(pDistanceDesc);
}
void LegController::setStepMode(LegStepMode stepMode) {
	assert( _state != kLegStateMakingStep );
	_stepMode = stepMode;
}
void LegController::setStepHeight(double stepHeight) {
	assert( _state != kLegStateMakingStep );
	_stepHeight = stepHeight;
}
void LegController::makeStep(Vector targetPosition, double duration) {
	switch (_state)	{
	case kLegStateBroken:
	case kLegStateMovingBy:
	case kLegStateMakingStep:
	case kLegStateManualControl:
		Print("Leg can not make step while moving/broken/manual controlled");
		return ;
	}
	_lastSteadyPosition = _targetStepPosition; // Remember position before step to restore it when step can not be finished
	_targetStepPosition = targetPosition;
	_stepDuration = duration;
	_state = kLegStateMakingStep;
	switch (_stepMode) {
	case kLegStepModeBlind:
		_makeBlindStep(targetPosition, duration);
		break;
	case kLegStepModeTouchAware:
		_stepAttemptInd = 0;
		break;
	case kLegStepModeDistanceAware:
		_makeDistanceAwareStep(targetPosition, duration);
		break;
	};
}
void LegController::_makeBlindStep(Vector targetPosition, double duration) {
	Vector currentPos = _positioner.targetPosition();
	Vector deltaPos = targetPosition - currentPos;
	Vector midPoint(currentPos + Vector(deltaPos.x / 2, deltaPos.y / 2, deltaPos.z + _stepHeight));
	MovementSequance pStepSequance;
	ArcPathZ path1(midPoint - currentPos);
	MoveBy stepPhase1(&path1, duration / 2.0, true, kCentralCS.affineMatrix());
	pStepSequance.addMovement(&stepPhase1);
	ArcPathZ path2(targetPosition - midPoint);
	MoveBy stepPhase2(&path2, duration / 2.0, true, kCentralCS.affineMatrix());
	pStepSequance.addMovement(&stepPhase2);
	_positioner.stopMovement();
	_positioner.startMovement(&pStepSequance);
}
Vector LegController::_targetPositionForTouchAwareStepForAttempt(const Vector &desiredPosition, int attemptInd) {
	Vector res(desiredPosition);
	if (attemptInd > 0) {
		Vector n(1.0), np(0.0, 1.0);
		if (desiredPosition.length() > kDistanceEpsilon) {
			n = desiredPosition.normalized();
			np.x = -n.y; np.y = n.x;
		}
		Vector delta;
		switch (attemptInd)
		{
		case 1:
			delta = n * 0.03;
			break;
		case 2:
			delta = np * 0.06;
			break;
		case 3:
			delta = -np * 0.06;
			break;
		default:
			delta = -n * 0.08;
			break;
		}
		res = res + delta;
		/*
		res.affineVector().print("Des pos");
		_positioner.tipCS().xAxis().affineVector().print("X");
		_positioner.tipCS().yAxis().affineVector().print("Y");
		_positioner.tipCS().zAxis().affineVector().print("Z");
		_positioner.tipCS().offset().affineVector().print("O"); */
	}
	return res;
}
void LegController::_makeTouchAwareStep(Vector targetPosition, double duration) {
	_positioner.stopMovement();

	Vector startPos = _positioner.targetPosition();
	Vector deltaPos = targetPosition - startPos; 
	MovementSequance stepSequance;

	// Phase 1: lift leg, arc
	Vector middlePos1(startPos + Vector(deltaPos.x / 2, deltaPos.y / 2, deltaPos.z + _stepHeight));
	ArcPathZ path1(middlePos1 - startPos);
	MoveBy stepPhase1(&path1, 0.3f * duration, true);
	stepSequance.addMovement(&stepPhase1);

	MovementSequance tillPressedSequance;
	// Phase 2: move forward and lower leg, arc
	Vector middlePos2(targetPosition + Vector(0.0, 0.0, _stepHeight * 0.5));
	ArcPathZ path2(middlePos2 - middlePos1);
	MoveBy stepPhase2(&path2, 0.3f * duration, true);
	tillPressedSequance.addMovement(&stepPhase2);

	// Phase 3: lower leg
	Vector endPos(targetPosition - Vector(0.0, 0.0, _stepHeight * 1.0));
	LinearPath path3(endPos - middlePos2);
	MoveBy stepPhase3(&path3, 0.8f * duration, true);
	tillPressedSequance.addMovement(&stepPhase3);
	MoveTillPressed tillPressed(&tillPressedSequance, &_touchSensor);

	stepSequance.addMovement(&tillPressed);
	_positioner.startMovement(&stepSequance);
}
void LegController::_makeDistanceAwareStep(Vector targetPostion, double duration) {
	// To be continued
}
void LegController::moveBy(Vector offset, double duration) {
	switch (_state)
	{
	case Hexapod::kLegStateBroken:
	case Hexapod::kLegStateMovingBy:
	case Hexapod::kLegStateMakingStep:
	case Hexapod::kLegStateManualControl:
		Print("Can not move leg while moving/broken/manual controlled");
		return ;
	}
	LinearPath path(offset);
	MoveBy movement(&path, duration, true, kCentralCS.affineMatrix());
	_positioner.stopMovement();
	_positioner.startMovement(&movement);
	_state = kLegStateMovingBy;
}
void LegController::update(double deltaTime) {
	_positioner.update(deltaTime);
	switch (_state)	{
	case Hexapod::kLegStateMovingBy:
		if ( ! _positioner.isMoving()) {
			_state = kLegStateSteadyOnGround;
		}
		break;
	case Hexapod::kLegStateMakingStep:
		switch (_stepMode) {
		case Hexapod::kLegStepModeBlind:
			if ( ! _positioner.isMoving() ) {
				_state = kLegStateSteadyOnGround;
			}
			break;
		case Hexapod::kLegStepModeTouchAware:
			_updateTouchAwareStep(deltaTime);
			break;
		case Hexapod::kLegStepModeDistanceAware:
			_updateDistanceAwareStep(deltaTime);
			break;
		}
		break;
	}
}
void LegController::_updateTouchAwareStep(double deltaTime) {
	if (_stepAttemptInd == 0) {
		Vector newTargetPos = _targetPositionForTouchAwareStepForAttempt(_targetStepPosition, _stepAttemptInd);
		_makeTouchAwareStep(newTargetPos, _stepDuration);
		++_stepAttemptInd;
	}
	else {
		if ( ! _positioner.isMoving()) {
			if ( ! _touchSensor.isPressed()) {
				if (_stepAttemptInd < kMaxStepAttemptsCount) { // Movement made but we are still not hitting floor, try again with new position
					Vector newTargetPos = _targetPositionForTouchAwareStepForAttempt(_targetStepPosition, _stepAttemptInd);
					_makeTouchAwareStep(newTargetPos, _stepDuration);
					++_stepAttemptInd;
				}
				else {
					_state = kLegStateCannotFinishStep; // Can not hit ground
				}
			}
			else {
				_state = kLegStateSteadyOnGround; // Step finished
			}
		}
	}
}
void LegController::_updateDistanceAwareStep(double deltaTime) {
	// Screw you, guys, I'm going home...
}
void LegController::restoreLastPosition() {
	if (_state == kLegStateBroken) {
		Print("Leg cannot restore position while broken");
		return;
	}
	_positioner.stopMovement();
	_state = kLegStateSteadyUnknown;
	Vector lastSteadyPositionTmp(_lastSteadyPosition); // Preserve last steady pos
	makeStep(_lastSteadyPosition, _stepDuration);
	_lastSteadyPosition = lastSteadyPositionTmp;
}
void LegController::setBroken(bool broken) {
	if (broken) {
		_positioner.stopMovement();
		_positioner.setPositionWithDuration(Vector(0.01f, 0.01f, 0.15f), 5.0f);
		_state = kLegStateBroken;
	}
	else {
		_state = kLegStateSteadyUnknown;
	}
}
void LegController::startManualControl() {
	if (_state == kLegStateBroken) {
		Print("Leg cannot be controlled while broken");
		return;
	}
	_positioner.stopMovement();
	_state = kLegStateManualControl;
}
void LegController::setTipPosition(Vector position, double duration) {
	if (_state != kLegStateManualControl) {
		Print("Cannot set tip position manually while not manual controlled");
		return;
	}
	_positioner.setPositionWithDuration(position, duration);
}
void LegController::stopManualControl() {
	if (_state == kLegStateManualControl) {
		_positioner.stopMovement();
		_state = kLegStateSteadyUnknown;
	}

}
