#include "MovementPath.h"
using namespace Hexapod;

LinearPath LinearPath::_instance(Vector(1.0, 1.0, 1.0));
Vector LinearPath::positionAtTime(double time) const {
	return Vector(_offset.x * time, _offset.y * time, _offset.z * time);
}

ArcPathZ ArcPathZ::_instance(Vector(1.0, 1.0, 1.0));
Vector ArcPathZ::positionAtTime(double time) const {
	if (_offset.z >= 0.0) {
		return Vector(
			_offset.x * (1.0 - std::cos(time * a90))
			, _offset.y * (1.0 - std::cos(time * a90))
			, _offset.z * std::sin(time * a90)
			);
	}
	else {
		return Vector(
			_offset.x * std::sin(time * a90)
			, _offset.y * std::sin(time * a90)
			, _offset.z * (1.0 - std::cos(time * a90))
			);
	}
}

StepPath::StepPath(Vector offset, double height)
	: _offset(offset), _height(height)
	, _midPosition(_offset.x/2, _offset.y/2, _offset.z + _height)
	, _phase1(_midPosition), _phase2(_offset - _midPosition) {
}
Vector StepPath::positionAtTime(double time) const {
	if (time < 0.5) {
		return _phase1.positionAtTime(2.0 * time);
	}
	else {
		return _phase2.positionAtTime(2.0 * (time - 0.5));
	}
}