#include "GeometricPrimitives.h"
#include <iomanip>
using namespace Hexapod;

const CoordinateSystem Hexapod::kGlobalCS(Vector(1.0, 0.0, 0.0), Vector(0.0, 1.0, 0.0), Vector(0.0, 0.0, 1.0), Vector(0.0, 0.0, 0.0), NULL);
const CoordinateSystem Hexapod::kCentralCS(Vector(1.0, 0.0, 0.0), Vector(0.0, 1.0, 0.0), Vector(0.0, 0.0, 1.0), Vector(0.0, 0.0, 0.0), NULL);

arma::vec4 Vector::affineVector() const {
	double buffer[] = {x, y, z, 1.0};
	return arma::vec4(buffer);
}
Vector Vector::fromPolar() const {
	return Vector(x * cos(y), x * sin(y), z);
}
Vector &Vector::operator+=(const Vector &v) {
	x += v.x; y += v.y; z += v.z;
	return *this;
}
Vector &Vector::operator-=(const Vector &v) {
	x -= v.x; y -= v.y; z -= v.z;
	return *this;
}
Vector &Vector::operator*=(double s) {
	x *= s; y *= s; z*= s;
	return *this;
}
Vector &Vector::operator/=(double s) {
	x /= s; y /= s; z /= s;
	return *this;
}
Vector Hexapod::operator+(const Vector &v1, const Vector &v2) {
	return Vector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}
Vector Hexapod::operator-(const Vector &v1, const Vector &v2) {
	return Vector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}
Vector Hexapod::operator*(const Vector &v1, const Vector &v2) {
	return Vector(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}
Vector Hexapod::operator/(const Vector &v1, const Vector &v2) {
	return Vector(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
}
Vector Hexapod::operator*(const Vector &v, double s) {
	return Vector(v.x*s, v.y*s, v.z*s);
}
Vector Hexapod::operator/(const Vector &v, double s) {
	return Vector(v.x/s, v.y/s, v.z/s);
}
//double Hexapod::operator*(const Vector &v1, const Vector &v2) {
//	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
//}
Vector Vector::operator-() {
	return Vector(-x, -y, -z);
}
double Vector::length() const {
	return std::sqrt(x*x + y*y + z*z);
}
double Vector::direction2D() const {
	double distance = std::max(length(), kDistanceEpsilon); // 0,1 mm is acceptable accuracy
	double sine = y / distance;
	double angle = std::asin(sine);
	if (x < 0 && angle > 0)
		angle = a180 - angle;
	if (x < 0 && angle < 0)
		angle = -a180 - angle;
	return angle;
}
Vector Vector::normalized() const {
	return *this / this->length();
}
double Vector::distance(const Vector &v) const {
	Vector delta = *this - v;
	return delta.length();
}
double Vector::dot(const Vector &v) const  {
	return x*v.x + y*v.y + z*v.z;
}
Vector Vector::cross(const Vector &v) const {
	return Vector(y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x);
}
void Vector::print(const char *prefix, const char *suffix) {
	std::cout << std::setiosflags(std::ios::fixed) << std::setprecision(3) 
		<< prefix 
		<< " " << std::setw(10) << x 
		<< " " << std::setw(10) << y 
		<< " " << std::setw(10) << z 
		<< suffix;
}


Vector Hexapod::GetAffineOffset(const Affine &affine) {
	arma::vec solution;
	bool solutionFound = arma::solve(solution, affine.submat(0,0, 2,2), affine.submat(0,3, 2,3));
	if ( ! solutionFound) {
		solution = affine.submat(0,3, 2,3);
	}
	return -Vector(solution, true);
}
void Hexapod::SetAffineOffset(Affine *pAffine, const Vector &offset) {
	Vector transformedOffset = Vector(offset.dot(Vector(pAffine->at(0, 0), pAffine->at(0, 1), pAffine->at(0, 2)))
									, offset.dot(Vector(pAffine->at(1, 0), pAffine->at(1, 1), pAffine->at(1, 2)))
									, offset.dot(Vector(pAffine->at(2, 0), pAffine->at(2, 1), pAffine->at(2, 2))) );
	pAffine->at(3,0) = transformedOffset.x;
	pAffine->at(3,1) = transformedOffset.y;
	pAffine->at(3,2) = transformedOffset.z;
}

CoordinateSystem::CoordinateSystem() {
	_parent = NULL;
	_affine.fill(0.0);
	_affine(0,0) = _affine(1,1) = _affine(2,2) = _affine(3,3) = 1.0;
}
CoordinateSystem::CoordinateSystem(const CoordinateSystem *parent): _parent(parent) {
	double affineBuffer[] = {
		1.0, 0.0, 0.0, 0.0, // column 0
		0.0, 1.0, 0.0, 0.0, // column 1
		0.0, 0.0, 1.0, 0.0, // column 2
		0.0, 0.0, 0.0, 1.0 // column 3
	};
	_affine = arma::mat44(affineBuffer);
}
CoordinateSystem::CoordinateSystem(Vector offset, const CoordinateSystem *parent): _parent(parent) {
	double affineBuffer[] = {
		1.0, 0.0, 0.0, 0.0, // column 0
		0.0, 1.0, 0.0, 0.0, // column 1
		0.0, 0.0, 1.0, 0.0, // column 2
		-offset.x, -offset.y, -offset.z, 1.0 // column 3
	};
	_affine = arma::mat44(affineBuffer);
}
CoordinateSystem::CoordinateSystem(Vector xAxis, Vector yAxis, Vector zAxis, Vector offset, const CoordinateSystem *parent): _parent(parent) {
	offset = Vector(offset.dot(xAxis), offset.dot(yAxis), offset.dot(zAxis)); // Affine transformation applies offset after rotation (or vector-axis projection), translate offset
	double affineBuffer[] = {
		xAxis.x, yAxis.x, zAxis.x, 0.0, // column 0
		xAxis.y, yAxis.y, zAxis.y, 0.0, // column 1
		xAxis.z, yAxis.z, zAxis.z, 0.0, // column 2
		-offset.x, -offset.y, -offset.z, 1.0 // column 3
	};
	_affine = arma::mat44(affineBuffer);
}
CoordinateSystem::CoordinateSystem(const arma::mat44 &affineMatrix, const CoordinateSystem *parent)
	:_affine(affineMatrix), _parent(parent) {
}
const arma::mat44 &CoordinateSystem::affineMatrix() const {
	return _affine;
}
Vector CoordinateSystem::xAxis() const {
	return Vector(_affine(0,0), _affine(0,1), _affine(0,2));
}
Vector CoordinateSystem::yAxis() const {
	return Vector(_affine(1,0), _affine(1,1), _affine(1,2));
}
Vector CoordinateSystem::zAxis() const {
	return Vector(_affine(2,0), _affine(2,1), _affine(2,2));
}
Vector CoordinateSystem::offset() const {
	return GetAffineOffset(_affine);//Vector(arma::vec4(_affine.i().eval().col(3)));
}
void CoordinateSystem::setOffset(Vector offset) {
	//offset = Vector(offset * xAxis(), offset * yAxis(), offset * zAxis());
	//_affine(3,0) = offset.x;
	//_affine(3,1) = offset.y;
	//_affine(3,2) = offset.z;
	SetAffineOffset(&_affine, offset);
}
CoordinateSystem CoordinateSystem::csRelativeToGlobalCS() const {
	if (_parent != NULL) {
		return *this * _parent->csRelativeToGlobalCS();
	}
	else {
		return *this;
	}
}
Vector CoordinateSystem::convertVectorFromCS(const CoordinateSystem &fromCS, const Vector &fromCSVector) const {
	CoordinateSystem thisRelativeToGlobal = csRelativeToGlobalCS();
	CoordinateSystem fromRelativeToGlobal = fromCS.csRelativeToGlobalCS();
	return thisRelativeToGlobal * fromRelativeToGlobal.inversed() * fromCSVector;
}
Vector CoordinateSystem::convertVectorToCS(const CoordinateSystem &toCS, const Vector &localVector) const {
	// from^(-1)*v = to^(-1)*v
	// cs^(-1)*localV = globalV
	CoordinateSystem thisRelativeToGlobal = csRelativeToGlobalCS();
	CoordinateSystem toRelativeToGlobal = toCS.csRelativeToGlobalCS();
	return toRelativeToGlobal * thisRelativeToGlobal.inversed() * localVector;
}
Affine CoordinateSystem::convertAffineFromCs(const CoordinateSystem &fromCS, const Affine &affine) const {
	CoordinateSystem thisRelativeToGlobal = csRelativeToGlobalCS();
	CoordinateSystem fromRelativeToGlobal = fromCS.csRelativeToGlobalCS();
	return (thisRelativeToGlobal * fromRelativeToGlobal.inversed()).affineMatrix() * affine;
}
Affine CoordinateSystem::convertAffineToCs(const CoordinateSystem &toCS, const Affine &affine) const {
	CoordinateSystem thisRelativeToGlobal = csRelativeToGlobalCS();
	CoordinateSystem toRelativeToGlobal = toCS.csRelativeToGlobalCS();
	return (toRelativeToGlobal * thisRelativeToGlobal.inversed()).affineMatrix() * affine;
}
void CoordinateSystem::setParentCS(const CoordinateSystem *newParentCS) {
	_affine = (csRelativeToGlobalCS() * newParentCS->csRelativeToGlobalCS().inversed()).affineMatrix();
	_parent = newParentCS;
}
CoordinateSystem CoordinateSystem::inversed() const {
	return CoordinateSystem(_affine.i().eval(), _parent);
}
CoordinateSystem Hexapod::operator*(const CoordinateSystem &cs1, const CoordinateSystem &cs2) {
	return CoordinateSystem(cs1.affineMatrix() * cs2.affineMatrix(), NULL);
}
Vector Hexapod::operator*(const CoordinateSystem &cs, const Vector &v) {
	return Vector(cs.affineMatrix() * v.affineVector());
}

CoordinateSystem CoordinateSystem::rotationCS(Vector rotationVector) {
	double angle = rotationVector.length();
	if (angle < a180 / 1800.0) {// 0.1 degree
		return CoordinateSystem(NULL);
	}
	else {
		double cose = std::cos(angle);
		double one_cose = 1.0 - cose;
		double sine = std::sin(angle);
		Vector n = rotationVector.normalized(); double nx=n.x, ny=n.y, nz = n.z;
		CoordinateSystem rotationCS( // Rotation about vector n (wiki), can be optimized (nx is always 0)
			Vector(cose+one_cose*nx*nx, one_cose*nx*ny-sine*nx, one_cose*nx*nz+sine*ny)
			, Vector(one_cose*ny*nx+sine*nz, cose+one_cose*ny*ny, one_cose*ny*ny-sine*nx)
			, Vector(one_cose*nx*nz-sine*ny, one_cose*nz*ny+sine*nx, cose+one_cose*nz*nz)
			, Vector(0.0), NULL
			);
		return rotationCS;
	}
}