#ifndef __HEXAPOD_LEG_POSITIONER_H__
#define __HEXAPOD_LEG_POSITIONER_H__

#include "Generic.h"
#include "GeometricPrimitives.h"
#include "ServoController.h"
#include "MovementBase.h"
#include <list>

namespace Hexapod {
	struct TipAreaDescriptor {
		double shAngleMin, shAngleMax;
		double svAngleMin, svAngleMax;
		double knAngleMin, knAngleMax;
		double bendedLegLength, bendedLegAngleOffset;
		double unbendedLegLength, unbendedLegAngleOffset;
		double angle1, angle2, angle3, angle4;
		Vector point1, point2, point3, point4, point5, point6;
	};

	class LegPositioner: public Movable {
	public:
		LegPositioner(const CoordinateSystem *pShoulderCS, const CoordinateSystem &tipCS);
		~LegPositioner() {SAFE_DELETE(_pMovement);}
		void initPhysicalServos(const PhysicalServoDescriptor *descs);
		void initWebotsServos(const WebotsServoDescriptor *descs);
		void update(double deltaTime);
		const CoordinateSystem &tipCS() const {return _tipCS;}
		void setTipCS(const CoordinateSystem &newTipCS);
		const CoordinateSystem *shoulderCS() {return _pShoulderCS;}
		bool isMoving() const {return (_pMovement != NULL && ! _pMovement->isFinished()) || false;}//_positioningDurationLeft > kTimeEpsilon;}
		void startMovement(MovementBase *pMovement);
		MovementBase *currentMovement() {return _pMovement;}
		void stopMovement() {SAFE_DELETE(_pMovement);}
		// Movable protocol
		virtual Vector targetPosition() const {return _targetPositionRelativeToTipCS;}
		virtual void setPositionWithDuration(const Vector &position, double duration);
	private:
		int _areaIndexForPositionX1Y1(Vector positionX1Y1) const;
		void _calculateTipArea();
		Vector _getNearestReachablePosition(const Vector &position);
		Vector _getNearestReachablePositionX1Y1(const Vector &positionX1Y1);
		void _servoAnglesForTipPosition(const Vector &position, double *anglesOut);
		void _servoAnglesForTipPositionX1Y1(const Vector &positionX1Y1, double *anglesOut);
	private:
		TipAreaDescriptor _area;
		double _positioningDurationLeft;
		ServoController _servoControllers[kLegServosCount];
		MovementBase *_pMovement;
		const CoordinateSystem *_pShoulderCS;
		CoordinateSystem _tipCS;
		Vector _targetPositionRelativeToShoulderCS; // Target position that was actually committed to the servos
		Vector _targetPositionRelativeToTipCS;
	};
}

#endif