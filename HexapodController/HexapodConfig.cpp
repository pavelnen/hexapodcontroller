#include "HexapodConfig.h"
using namespace Hexapod;

const int Hexapod::kTimeStep = 48;

const LegIndex Hexapod::kFirstLeg = 0;
const LegIndex Hexapod::FL = 0;
const LegIndex Hexapod::ML = 1;
const LegIndex Hexapod::RL = 2;
const LegIndex Hexapod::RR = 3;
const LegIndex Hexapod::MR = 4;
const LegIndex Hexapod::FR = 5;
const LegIndex Hexapod::kLastLeg = 5;
//const int Hexapod::kLegsCount = 6;

const double Hexapod::kLegsDirections[6] = { 3.1416/6.0, 3.1416/2.0, 3.1416*5.0/6.0, -3.1416*5.0/6.0, -3.1416/2.0, -3.1416/6.0};

const LegServoIndex Hexapod::SH = 0;
const LegServoIndex Hexapod::SV = 1;
const LegServoIndex Hexapod::KN = 2;
//const int kLegServosCount = 3;

const double Hexapod::kShoulderLength = 0.038;
const double Hexapod::kShoulderHeight = 0.013;
const double Hexapod::kFemurLength =0.057;
const double Hexapod::kTibiaLength = 0.125;//12.4;
const double Hexapod::kBodyRadius = 0.137;
//const double Hexapod::kBodyHeight = 0.135;
//const double Hexapod::kTipRadialOffset = 0.0;
const double Hexapod::kBodyHeight = 0.11;
const double Hexapod::kTipRadialOffset = 0.025;
const double Hexapod::kTipRadialDistance = kBodyRadius + kShoulderLength + kFemurLength + kTipRadialOffset;
const double Hexapod::kBodyCenterToShoulderVerticalOffset = 0.015;

const char *Hexapod::kServoNames [6][3] = {
	{"fl_sh", "fl_sv", "fl_k"},
	{"ml_sh", "ml_sv", "ml_k"},
	{"rl_sh", "rl_sv", "rl_k"},
	{"rr_sh", "rr_sv", "rr_k"},
	{"mr_sh", "mr_sv", "mr_k"},
	{"fr_sh", "fr_sv", "fr_k"}
};
const char *Hexapod::kTouchesNames[6] = {
	"fl_ts", "ml_ts", "rl_ts", "rr_ts", "mr_ts", "fr_ts"
};